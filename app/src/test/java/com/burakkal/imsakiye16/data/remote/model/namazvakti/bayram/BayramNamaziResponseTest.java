package com.burakkal.imsakiye16.data.remote.model.namazvakti.bayram;

import org.junit.Before;
import org.junit.Test;
import org.simpleframework.xml.core.Persister;

import static org.junit.Assert.*;

/**
 * Created by Burak on 13.5.2018.
 * burakkal54@gmail.com
 */
public class BayramNamaziResponseTest {

    private static final String BAYRAM_NAMAZI_RESPONSE =
            "<s:Envelope xmlns:s=\"http://schemas.xmlsoap.org/soap/envelope/\">\n" +
            "   <s:Body>\n" +
            "      <BayramNamaziVaktiResponse xmlns=\"http://tempuri.org/\">\n" +
            "         <BayramNamaziVaktiResult xmlns:a=\"http://schemas.datacontract.org/2004/07/PrayTimes.Services.Entities\" xmlns:i=\"http://www.w3.org/2001/XMLSchema-instance\">\n" +
            "            <a:KurbanBayramNamaziHTarihi>10 Zilhicce 1439</a:KurbanBayramNamaziHTarihi>\n" +
            "            <a:KurbanBayramNamaziSaati>06:55:00</a:KurbanBayramNamaziSaati>\n" +
            "            <a:KurbanBayramNamaziTarihi>21 Ağustos 2018 Salı</a:KurbanBayramNamaziTarihi>\n" +
            "            <a:RamazanBayramNamaziHTarihi>1 Şevval 1439</a:RamazanBayramNamaziHTarihi>\n" +
            "            <a:RamazanBayramNamaziSaati>06:12:00</a:RamazanBayramNamaziSaati>\n" +
            "            <a:RamazanBayramNamaziTarihi>15 Haziran 2018 Cuma</a:RamazanBayramNamaziTarihi>\n" +
            "         </BayramNamaziVaktiResult>\n" +
            "      </BayramNamaziVaktiResponse>\n" +
            "   </s:Body>\n" +
            "</s:Envelope>";
    private Persister persister;

    @Before
    public void setUp() throws Exception {
        persister = new Persister();
    }

    @Test
    public void testBayramNamaziResponse() throws Exception {
        BayramNamaziResponse response = persister.read(
                BayramNamaziResponse.class, BAYRAM_NAMAZI_RESPONSE);

        assertEquals(response.getRamazanHicriTarih(), "1 Şevval 1439");
        assertEquals(response.getRamazanTarih(), "15 Haziran 2018 Cuma");
        assertEquals(response.getRamazanSaati(), "06:12:00");
        assertEquals(response.getKurbanHicriTarih(), "10 Zilhicce 1439");
        assertEquals(response.getKurbanTarih(), "21 Ağustos 2018 Salı");
        assertEquals(response.getKurbanSaati(), "06:55:00");
    }
}