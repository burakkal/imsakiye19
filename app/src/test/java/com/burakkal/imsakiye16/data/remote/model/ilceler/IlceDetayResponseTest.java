package com.burakkal.imsakiye16.data.remote.model.ilceler;

import org.junit.Before;
import org.junit.Test;
import org.simpleframework.xml.core.Persister;

import static org.junit.Assert.*;

/**
 * Created by Burak on 11.5.2018.
 * burakkal54@gmail.com
 */
public class IlceDetayResponseTest {

    private static final String ILCE_DETAY_RESPONSE =
            "<s:Envelope xmlns:s=\"http://schemas.xmlsoap.org/soap/envelope/\">\n" +
            "   <s:Body>\n" +
            "      <IlceBilgisiDetayResponse xmlns=\"http://tempuri.org/\">\n" +
            "         <IlceBilgisiDetayResult xmlns:a=\"http://schemas.datacontract.org/2004/07/PrayTimes.Services.Entities\" xmlns:i=\"http://www.w3.org/2001/XMLSchema-instance\">\n" +
            "            <a:CografiKibleAcisi>154</a:CografiKibleAcisi>\n" +
            "            <a:IlceAdi>SAKARYA</a:IlceAdi>\n" +
            "            <a:IlceAdiEn i:nil=\"true\"/>\n" +
            "            <a:IlceID>9807</a:IlceID>\n" +
            "            <a:KabeyeUzaklik>2327</a:KabeyeUzaklik>\n" +
            "            <a:KibleAcisi>150</a:KibleAcisi>\n" +
            "            <a:SehirAdi>SAKARYA</a:SehirAdi>\n" +
            "            <a:SehirAdiEn i:nil=\"true\"/>\n" +
            "            <a:UlkeAdi>TÜRKİYE</a:UlkeAdi>\n" +
            "            <a:UlkeAdiEn i:nil=\"true\"/>\n" +
            "         </IlceBilgisiDetayResult>\n" +
            "      </IlceBilgisiDetayResponse>\n" +
            "   </s:Body>\n" +
            "</s:Envelope>";
    private Persister persister;

    @Before
    public void setUp() throws Exception {
        persister = new Persister();
    }

    @Test
    public void testIlceDetayResponse() throws Exception {
        IlceDetayResponse response = persister.read(IlceDetayResponse.class, ILCE_DETAY_RESPONSE);

        assertEquals(response.getCografiKibleAcisi(), 154);
        assertEquals(response.getKibleAcisi(), 150);
        assertEquals(response.getKabeyeUzaklik(), 2327);
    }
}