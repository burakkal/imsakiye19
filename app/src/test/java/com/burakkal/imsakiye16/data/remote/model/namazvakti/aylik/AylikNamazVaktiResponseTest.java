package com.burakkal.imsakiye16.data.remote.model.namazvakti.aylik;

import org.junit.Before;
import org.junit.Test;
import org.simpleframework.xml.core.Persister;

import static org.junit.Assert.*;

/**
 * Created by Burak on 13.5.2018.
 * burakkal54@gmail.com
 */
public class AylikNamazVaktiResponseTest {

    private static final String AYLIK_NAMAZ_VAKTI_RESPONSE =
            "<s:Envelope xmlns:s=\"http://schemas.xmlsoap.org/soap/envelope/\">\n" +
            "   <s:Body>\n" +
            "      <AylikNamazVaktiResponse xmlns=\"http://tempuri.org/\">\n" +
            "         <AylikNamazVaktiResult xmlns:a=\"http://schemas.datacontract.org/2004/07/PrayTimes.Services.Entities\" xmlns:i=\"http://www.w3.org/2001/XMLSchema-instance\">\n" +
            "            <a:NamazVakti>\n" +
            "               <a:Aksam>20:13</a:Aksam>\n" +
            "               <a:AyinSekliURL>http://namazvakti.diyanet.gov.tr/images/sd5.gif</a:AyinSekliURL>\n" +
            "               <a:Gunes>05:36</a:Gunes>\n" +
            "               <a:GunesBatis>20:06</a:GunesBatis>\n" +
            "               <a:GunesDogus>05:43</a:GunesDogus>\n" +
            "               <a:HicriTarihKisa>27.8.1439</a:HicriTarihKisa>\n" +
            "               <a:HicriTarihKisaIso8601 i:nil=\"true\"/>\n" +
            "               <a:HicriTarihUzun>27 Şaban 1439</a:HicriTarihUzun>\n" +
            "               <a:HicriTarihUzunIso8601 i:nil=\"true\"/>\n" +
            "               <a:Ikindi>16:54</a:Ikindi>\n" +
            "               <a:Imsak>03:52</a:Imsak>\n" +
            "               <a:KibleSaati>12:12</a:KibleSaati>\n" +
            "               <a:MiladiTarihKisa>13.05.2018</a:MiladiTarihKisa>\n" +
            "               <a:MiladiTarihKisaIso8601>13.05.2018</a:MiladiTarihKisaIso8601>\n" +
            "               <a:MiladiTarihUzun>13 Mayıs 2018 Pazar</a:MiladiTarihUzun>\n" +
            "               <a:MiladiTarihUzunIso8601>2018-05-13T00:00:00.0000000+00:00</a:MiladiTarihUzunIso8601>\n" +
            "               <a:Ogle>13:01</a:Ogle>\n" +
            "               <a:Yatsi>21:50</a:Yatsi>\n" +
            "            </a:NamazVakti>\n" +
            "            <a:NamazVakti>\n" +
            "               <a:Aksam>20:14</a:Aksam>\n" +
            "               <a:AyinSekliURL>http://namazvakti.diyanet.gov.tr/images/sd6.gif</a:AyinSekliURL>\n" +
            "               <a:Gunes>05:35</a:Gunes>\n" +
            "               <a:GunesBatis>20:07</a:GunesBatis>\n" +
            "               <a:GunesDogus>05:42</a:GunesDogus>\n" +
            "               <a:HicriTarihKisa>28.8.1439</a:HicriTarihKisa>\n" +
            "               <a:HicriTarihKisaIso8601 i:nil=\"true\"/>\n" +
            "               <a:HicriTarihUzun>28 Şaban 1439</a:HicriTarihUzun>\n" +
            "               <a:HicriTarihUzunIso8601 i:nil=\"true\"/>\n" +
            "               <a:Ikindi>16:54</a:Ikindi>\n" +
            "               <a:Imsak>03:50</a:Imsak>\n" +
            "               <a:KibleSaati>12:13</a:KibleSaati>\n" +
            "               <a:MiladiTarihKisa>14.05.2018</a:MiladiTarihKisa>\n" +
            "               <a:MiladiTarihKisaIso8601>14.05.2018</a:MiladiTarihKisaIso8601>\n" +
            "               <a:MiladiTarihUzun>14 Mayıs 2018 Pazartesi</a:MiladiTarihUzun>\n" +
            "               <a:MiladiTarihUzunIso8601>2018-05-14T00:00:00.0000000+00:00</a:MiladiTarihUzunIso8601>\n" +
            "               <a:Ogle>13:01</a:Ogle>\n" +
            "               <a:Yatsi>21:51</a:Yatsi>\n" +
            "            </a:NamazVakti>\n" +
            "         </AylikNamazVaktiResult>\n" +
            "      </AylikNamazVaktiResponse>\n" +
            "   </s:Body>\n" +
            "</s:Envelope>";
    private Persister persister;

    @Before
    public void setUp() throws Exception {
        persister = new Persister();
    }

    @Test
    public void testAylikNamazVaktiResponse() throws Exception {
        AylikNamazVaktiResponse response = persister.read(
                AylikNamazVaktiResponse.class, AYLIK_NAMAZ_VAKTI_RESPONSE);

        assertEquals(2, response.getNamazVakitleri().size());

        NamazVakti namazVakti = response.getNamazVakitleri().get(0);

        assertEquals(namazVakti.getImsak(), "03:52");
        assertEquals(namazVakti.getGunes(), "05:36");
        assertEquals(namazVakti.getOgle(), "13:01");
        assertEquals(namazVakti.getIkindi(), "16:54");
        assertEquals(namazVakti.getAksam(), "20:13");
        assertEquals(namazVakti.getYatsi(), "21:50");
        assertEquals(namazVakti.getGunesBatis(), "20:06");
        assertEquals(namazVakti.getGunesDogus(), "05:43");
        assertEquals(namazVakti.getKibleSaati(), "12:12");
        assertEquals(namazVakti.getHicriTarih(), "27.8.1439");
        assertEquals(namazVakti.getMiladiTarih(), "13.05.2018");
        assertEquals(namazVakti.getAyinSekliUrl(), "http://namazvakti.diyanet.gov.tr/images/sd5.gif");
    }
}