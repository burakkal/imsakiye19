package com.burakkal.imsakiye16.ui.ayarlar

enum class VakitHatirlatici(val code: Int) {
    SAHUR_UYANDIRMA(0), IMSAK_UYARI(1), IFTAR_UYARI(2)
}