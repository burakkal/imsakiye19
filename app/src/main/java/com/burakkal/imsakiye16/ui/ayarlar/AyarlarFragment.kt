package com.burakkal.imsakiye16.ui.ayarlar

import android.content.Context
import android.media.AudioManager
import android.net.Uri
import android.os.Bundle
import android.text.InputType
import androidx.annotation.StringRes
import androidx.preference.EditTextPreference
import androidx.preference.Preference
import androidx.preference.PreferenceFragmentCompat
import androidx.preference.SwitchPreferenceCompat
import com.burakkal.imsakiye16.ImsakiyeApp
import com.burakkal.imsakiye16.R
import com.burakkal.imsakiye16.data.pref.hatirlatici.HatirlaticiPreferences
import com.burakkal.imsakiye16.ui.alarm.AlarmHelper
import com.burakkal.imsakiye16.util.getRawUri
import com.burakkal.imsakiye16.util.showToast
import timber.log.Timber
import xyz.aprildown.ultimatemusicpicker.MusicPickerListener
import xyz.aprildown.ultimatemusicpicker.UltimateMusicPicker
import javax.inject.Inject

class AyarlarFragment : PreferenceFragmentCompat(), MusicPickerListener {

    @Inject lateinit var hatirlaticiPreferences: HatirlaticiPreferences
    @Inject lateinit var alarmHelper: AlarmHelper
    private lateinit var vakitHatirlatici: VakitHatirlatici

    override fun onAttach(context: Context) {
        super.onAttach(context)
        ImsakiyeApp.get(activity).component.inject(this)
    }

    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        setPreferencesFromResource(R.xml.preferences, rootKey)
        initSahurAyarlar()
        initImsakAyarlar()
        initIftarAyarlar()

        // COMPLETED: her biri için listener ekle ve duruma göre alarmı iptal et
        // COMPLETED: sahur uyandırma süresini 1-240 dakika arası ayarla
        // COMPLETED: Sahur uyandırma süresinin değişmesine göre alarmı güncelle
    }

    override fun onDestroy() {
        super.onDestroy()
        alarmHelper.unsubscribe()
    }

    private fun initSahurAyarlar() {
        val prefSahurUyandirma =
                findPreference<SwitchPreferenceCompat>(R.string.pref_sahur_uyandirma_key)
        val prefSahurUyandirmaSuresi =
                findPreference<EditTextPreference>(R.string.pref_sahur_uyandirma_suresi_key)
        val prefSahurUyandirmaSesi =
                findPreference<Preference>(R.string.pref_sahur_uyandirma_sesi_key)
        if (prefSahurUyandirma == null || prefSahurUyandirmaSuresi == null
                || prefSahurUyandirmaSesi == null) return

        prefSahurUyandirmaSuresi.setOnBindEditTextListener { editText ->
            editText.inputType = InputType.TYPE_CLASS_NUMBER
        }
        prefSahurUyandirmaSuresi.isEnabled = prefSahurUyandirma.isChecked
        prefSahurUyandirmaSuresi.setOnPreferenceChangeListener { pref, newValue ->
            val surePref = pref as EditTextPreference
            val sureValue = newValue as String
            Timber.d("prefSahurUyandirmaSuresi.setOnPreferenceChangeListener[sureValueStr = %s]", sureValue)
            var sureValueInt = 60
            try {
                sureValueInt = sureValue.toInt()
            } catch (e: Exception) {
                Timber.e("sureValueInt = %s", e.localizedMessage)
                surePref.text = "60"
                alarmHelper.setAlarmSahurUyandirma(sureValueInt)
                return@setOnPreferenceChangeListener false
            }
            Timber.d("prefSahurUyandirmaSuresi.setOnPreferenceChangeListener[sureValueInt = %d]", sureValueInt)
            if (sureValueInt < 1) {
                sureValueInt = 1
                surePref.text = "1"
                showToast(activity, "Alarm 1-240 dakika öncesine kurulabilir")
                alarmHelper.setAlarmSahurUyandirma(sureValueInt)
                return@setOnPreferenceChangeListener false
            } else if (sureValueInt > 240) {
                sureValueInt = 240
                surePref.text = "240"
                showToast(activity, "Alarm 1-240 dakika öncesine kurulabilir")
                alarmHelper.setAlarmSahurUyandirma(sureValueInt)
                return@setOnPreferenceChangeListener false
            }
            alarmHelper.setAlarmSahurUyandirma(sureValueInt)
            true
        }

        prefSahurUyandirmaSesi.isEnabled = prefSahurUyandirma.isChecked
        prefSahurUyandirmaSesi.summary = hatirlaticiPreferences.sahurUyandirmaSesiAdi

        prefSahurUyandirma.setOnPreferenceChangeListener { _, newValue ->
            val isChecked = newValue as Boolean
            prefSahurUyandirmaSuresi.isEnabled = isChecked
            prefSahurUyandirmaSesi.isEnabled = isChecked
            if (isChecked) alarmHelper.setAlarmSahurUyandirma()
            else alarmHelper.cancelAlarmSahurUyandirma()
            true
        }
    }

    private fun initImsakAyarlar() {
        val prefImsakUyari = findPreference<SwitchPreferenceCompat>(R.string.pref_imsak_uyari_key)
        val prefImsakUyariSesi = findPreference<Preference>(R.string.pref_imsak_uyari_sesi_key)
        if (prefImsakUyari == null || prefImsakUyariSesi == null) return

        prefImsakUyariSesi.isEnabled = prefImsakUyari.isChecked
        prefImsakUyariSesi.summary = hatirlaticiPreferences.imsakUyariSesiAdi

        prefImsakUyari.setOnPreferenceChangeListener { _, newValue ->
            val isChecked = newValue as Boolean
            prefImsakUyariSesi.isEnabled = isChecked
            if (isChecked) alarmHelper.setAlarmImsakUyari()
            else alarmHelper.cancelAlarmImsakUyari()
            true
        }
    }

    private fun initIftarAyarlar() {
        val prefIftarUyari = findPreference<SwitchPreferenceCompat>(R.string.pref_iftar_uyari_key)
        val prefIftarUyariSesi = findPreference<Preference>(R.string.pref_iftar_uyari_sesi_key)
        if (prefIftarUyari == null || prefIftarUyariSesi == null) return

        prefIftarUyariSesi.isEnabled = prefIftarUyari.isChecked
        prefIftarUyariSesi.summary = hatirlaticiPreferences.iftarUyariSesiAdi

        prefIftarUyari.setOnPreferenceChangeListener { _, newValue ->
            val isChecked = newValue as Boolean
            prefIftarUyariSesi.isEnabled = isChecked
            if (isChecked) alarmHelper.setAlarmIftarUyari()
            else alarmHelper.cancelAlarmIftarUyari()
            true
        }
    }

    override fun onPreferenceTreeClick(preference: Preference?): Boolean {
        if (preference == null || !preference.hasKey()) return false
        return when {
            preference.key.contentEquals(getString(R.string.pref_sahur_uyandirma_sesi_key)) -> {
                showSahurUyandirmaDialog()
                true
            }
            preference.key.contentEquals(getString(R.string.pref_imsak_uyari_sesi_key)) -> {
                showImsakUyariDialog()
                true
            }
            preference.key.contentEquals(getString(R.string.pref_iftar_uyari_sesi_key)) -> {
                showIftarUyariDialog()
                true
            }
            else -> super.onPreferenceTreeClick(preference)
        }
    }

    private fun showSahurUyandirmaDialog() {
        vakitHatirlatici = VakitHatirlatici.SAHUR_UYANDIRMA
        val defaultUri = getRawUri(R.raw.ramazan_davulu, context!!)
        val selectedUri = hatirlaticiPreferences.getSahurUyandirmaSesi(defaultUri.toString())
        UltimateMusicPicker()
                .windowTitle(getString(R.string.ses_tonu))
                .streamType(AudioManager.STREAM_ALARM)
                .removeSilent()
                .defaultTitleAndUri(getString(R.string.ramazan_davulu), getRawUri(R.raw.ramazan_davulu, context!!))
                .selectUri(selectedUri)
                .additional(getString(R.string.ramazan_davulu_mani), getRawUri(R.raw.ramazan_davulu_mani, context!!))
                .additional(getString(R.string.mekke_ezan), getRawUri(R.raw.makkah_azan, context!!))
                .alarm()
                .goWithDialog(childFragmentManager)
    }

    private fun showImsakUyariDialog() {
        vakitHatirlatici = VakitHatirlatici.IMSAK_UYARI
        val defaultUri = getRawUri(R.raw.makkah_azan, context!!)
        val selectedUri = hatirlaticiPreferences.getImsakUyariSesi(defaultUri.toString())
        UltimateMusicPicker()
                .windowTitle(getString(R.string.ses_tonu))
                .streamType(AudioManager.STREAM_ALARM)
                .removeSilent()
                .defaultTitleAndUri(getString(R.string.mekke_ezan), getRawUri(R.raw.makkah_azan, context!!))
                .additional(getString(R.string.ramazan_davulu), getRawUri(R.raw.ramazan_davulu, context!!))
                .additional(getString(R.string.ramazan_davulu_mani), getRawUri(R.raw.ramazan_davulu_mani, context!!))
                .selectUri(selectedUri)
                .goWithDialog(childFragmentManager)
    }

    private fun showIftarUyariDialog() {
        vakitHatirlatici = VakitHatirlatici.IFTAR_UYARI
        val defaultUri = getRawUri(R.raw.makkah_azan, context!!)
        val selectedUriString = hatirlaticiPreferences.getIftarUyariSesi(defaultUri.toString())
        UltimateMusicPicker()
                .windowTitle(getString(R.string.ses_tonu))
                .streamType(AudioManager.STREAM_ALARM)
                .removeSilent()
                .defaultTitleAndUri(getString(R.string.mekke_ezan), getRawUri(R.raw.makkah_azan, context!!))
                .additional(getString(R.string.ramazan_davulu), getRawUri(R.raw.ramazan_davulu, context!!))
                .additional(getString(R.string.ramazan_davulu_mani), getRawUri(R.raw.ramazan_davulu_mani, context!!))
                .selectUri(selectedUriString)
                .goWithDialog(childFragmentManager)
    }

    override fun onMusicPick(uri: Uri, title: String) {
        when (vakitHatirlatici) {
            VakitHatirlatici.SAHUR_UYANDIRMA -> updateSahurUyandirmaPref(uri, title)
            VakitHatirlatici.IMSAK_UYARI -> updateImsakUyariPref(uri, title)
            VakitHatirlatici.IFTAR_UYARI -> updateIftarUyariPref(uri, title)
        }
    }

    private fun updateSahurUyandirmaPref(uri: Uri, title: String) {
        hatirlaticiPreferences.setSahurUyandirmaSesi(uri.toString())
        hatirlaticiPreferences.sahurUyandirmaSesiAdi = title
        val sahurUyandirmaPref = findPreference<Preference>(R.string.pref_sahur_uyandirma_sesi_key)
        sahurUyandirmaPref?.summary = hatirlaticiPreferences.sahurUyandirmaSesiAdi
    }

    private fun updateImsakUyariPref(uri: Uri, title: String) {
        hatirlaticiPreferences.setImsakUyariSesi(uri.toString())
        hatirlaticiPreferences.imsakUyariSesiAdi = title
        val imsakUyariPref = findPreference<Preference>(R.string.pref_imsak_uyari_sesi_key)
        imsakUyariPref?.summary = hatirlaticiPreferences.imsakUyariSesiAdi
    }

    private fun updateIftarUyariPref(uri: Uri, title: String) {
        hatirlaticiPreferences.setIftarUyariSesi(uri.toString())
        hatirlaticiPreferences.iftarUyariSesiAdi = title
        val imsakUyariPref = findPreference<Preference>(R.string.pref_iftar_uyari_sesi_key)
        imsakUyariPref?.summary = hatirlaticiPreferences.iftarUyariSesiAdi
    }

    override fun onPickCanceled() {

    }

    private fun <T : Preference> findPreference(@StringRes res: Int): T? =
            preferenceManager.findPreference(getString(res))

}