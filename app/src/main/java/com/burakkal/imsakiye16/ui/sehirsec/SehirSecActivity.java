package com.burakkal.imsakiye16.ui.sehirsec;

import android.os.Bundle;
import android.widget.TextView;

import com.burakkal.imsakiye16.R;
import com.burakkal.imsakiye16.ui.sehirsec.ulke.UlkeSecFragment;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import butterknife.BindView;
import butterknife.ButterKnife;

public class SehirSecActivity extends AppCompatActivity {

    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.toolbar_title) TextView toolbarTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sehir_sec);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        toolbarTitle.setText("Şehir seçiniz");
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.fragment_container_sehir_sec, new UlkeSecFragment())
                    .commit();
        }
    }
}
