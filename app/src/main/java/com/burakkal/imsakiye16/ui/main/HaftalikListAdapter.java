package com.burakkal.imsakiye16.ui.main;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.burakkal.imsakiye16.R;
import com.burakkal.imsakiye16.data.local.entity.TblNamazVakti;
import com.burakkal.imsakiye16.util.DateUtils;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Burak on 15.5.2018.
 * burakkal54@gmail.com
 */
public class HaftalikListAdapter extends RecyclerView.Adapter<HaftalikListAdapter.HaftalikViewHolder> {

    private List<TblNamazVakti> vakitler;

    public HaftalikListAdapter(List<TblNamazVakti> vakitler) {
        this.vakitler = vakitler;
    }

    @NonNull
    @Override
    public HaftalikViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_vakitler, parent, false);
        return new HaftalikViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull HaftalikViewHolder holder, int position) {
        TblNamazVakti vakit = vakitler.get(position);

        holder.tvTarih.setText(vakit.getHicriTarih().substring(0, vakit.getHicriTarih().lastIndexOf(" ")));
        holder.tvImsak.setText(DateUtils.fromDate(vakit.getImsak(), DateUtils.FORMAT_HOUR));
        holder.tvGunes.setText(DateUtils.fromDate(vakit.getGunes(), DateUtils.FORMAT_HOUR));
        holder.tvOgle.setText(DateUtils.fromDate(vakit.getOgle(), DateUtils.FORMAT_HOUR));
        holder.tvIkindi.setText(DateUtils.fromDate(vakit.getIkindi(), DateUtils.FORMAT_HOUR));
        holder.tvAksam.setText(DateUtils.fromDate(vakit.getAksam(), DateUtils.FORMAT_HOUR));
        holder.tvYatsi.setText(DateUtils.fromDate(vakit.getYatsi(), DateUtils.FORMAT_HOUR));

        if (DateUtils.hasDatePassed(vakit.getMiladiTarih())) {
            holder.itemView.setBackgroundResource(R.color.grey);
        } else {
            holder.itemView.setBackgroundResource(position % 2 == 0
                    ? R.color.white
                    : R.color.card_background);
        }
    }

    @Override
    public int getItemCount() {
        return vakitler != null ? vakitler.size() : 0;
    }

    class HaftalikViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_list_row_tarih) TextView tvTarih;
        @BindView(R.id.tv_list_row_imsak) TextView tvImsak;
        @BindView(R.id.tv_list_row_gunes) TextView tvGunes;
        @BindView(R.id.tv_list_row_ogle) TextView tvOgle;
        @BindView(R.id.tv_list_row_ikindi) TextView tvIkindi;
        @BindView(R.id.tv_list_row_aksam) TextView tvAksam;
        @BindView(R.id.tv_list_row_yatsi) TextView tvYatsi;

        public HaftalikViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}
