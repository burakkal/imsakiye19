package com.burakkal.imsakiye16.ui.alarm

import android.content.Context
import android.util.Log
import com.burakkal.imsakiye16.data.local.ImsakiyeDatabase
import com.burakkal.imsakiye16.data.pref.hatirlatici.HatirlaticiPreferences
import com.burakkal.imsakiye16.ui.ayarlar.VakitHatirlatici
import com.burakkal.imsakiye16.util.alarmAyarla
import com.burakkal.imsakiye16.util.alarmIptal
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import timber.log.Timber
import java.util.*

class AlarmHelper(private val context: Context, private val db: ImsakiyeDatabase,
                  private val hatirlaticiPref: HatirlaticiPreferences) {

    private val compositeDisposable: CompositeDisposable = CompositeDisposable()
    var completionListener: CompletionListener? = null

    interface CompletionListener {
        fun onComplete()
        fun onError(error: Throwable)
    }

    fun setAlarmSahurUyandirma(minBefore: Int = hatirlaticiPref.sahurUyandirmaSuresi) {
        Timber.d("[T]setAlarmSahurUyandirma = " + minBefore)
        Log.d("AlarmHelperKt", "[L]setAlarmSahurUyandirma = " + minBefore)
        val sahurUyandirmaLimit = Calendar.getInstance()
                .apply { add(Calendar.MINUTE, minBefore) }
                .time
        val disposable = db.konumDao().lastAddedKonum
                .subscribeOn(Schedulers.io())
                .flatMap { db.namazVaktiDao().getSiradakiImsak(it.ilceId, sahurUyandirmaLimit) }
                .subscribe(
                        { imsak ->
                            alarmAyarla(context, VakitHatirlatici.SAHUR_UYANDIRMA,
                                    getMinuteAddedTime(imsak, minBefore * -1))
                            completionListener?.onComplete()
                        },
                        { t ->
                            Timber.e(t)
                            completionListener?.onError(t)
                        })
        compositeDisposable.add(disposable)
    }

    private fun getMinuteAddedTime(date: Date? = Date(System.currentTimeMillis()),
                                   minute: Int): Date? {
        if (date == null) return null
        return Calendar.getInstance().apply {
            time = date
            add(Calendar.MINUTE, minute)
        }.time
    }

    fun setAlarmImsakUyari() {
        val disposable = db.konumDao().lastAddedKonum
                .subscribeOn(Schedulers.io())
                .flatMap { db.namazVaktiDao().getSiradakiImsak(it.ilceId, Calendar.getInstance().time) }
                .subscribe(
                        { imsak ->
                            alarmAyarla(context, VakitHatirlatici.IMSAK_UYARI, imsak)
                            completionListener?.onComplete()
                        },
                        { t ->
                            Timber.e(t)
                            completionListener?.onError(t)
                        })
        compositeDisposable.add(disposable)
    }

    fun setAlarmIftarUyari() {
        val disposable = db.konumDao().lastAddedKonum
                .subscribeOn(Schedulers.io())
                .flatMap { db.namazVaktiDao().getSiradakiAksam(it.ilceId, Calendar.getInstance().time) }
                .subscribe(
                        { aksam ->
                            alarmAyarla(context, VakitHatirlatici.IFTAR_UYARI, aksam)
                            completionListener?.onComplete()
                        },
                        { t ->
                            Timber.e(t)
                            completionListener?.onError(t)
                        })
        compositeDisposable.add(disposable)
    }

    fun cancelAlarmSahurUyandirma() {
        alarmIptal(context, VakitHatirlatici.SAHUR_UYANDIRMA)
    }

    fun cancelAlarmImsakUyari() {
        alarmIptal(context, VakitHatirlatici.IMSAK_UYARI)
    }

    fun cancelAlarmIftarUyari() {
        alarmIptal(context, VakitHatirlatici.IFTAR_UYARI)
    }

    fun unsubscribe() {
        compositeDisposable.clear()
    }
}