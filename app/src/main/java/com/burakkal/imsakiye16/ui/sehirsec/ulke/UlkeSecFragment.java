package com.burakkal.imsakiye16.ui.sehirsec.ulke;

import android.app.ProgressDialog;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.burakkal.imsakiye16.ImsakiyeApp;
import com.burakkal.imsakiye16.R;
import com.burakkal.imsakiye16.data.ImsakiyeRepository;
import com.burakkal.imsakiye16.data.remote.model.ulkeler.Ulke;
import com.burakkal.imsakiye16.ui.sehirsec.sehir.SehirSecFragment;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnItemClick;
import butterknife.Unbinder;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import timber.log.Timber;

/**
 * Created by Burak on 7.5.2018.
 * burakkal54@gmail.com
 */
public class UlkeSecFragment extends Fragment {

    @BindView(R.id.list_sehir) ListView lvUlke;
    @Inject ImsakiyeRepository repository;

    private Unbinder unbinder;
    private ProgressDialog pd;
    private CompositeDisposable compositeDisposable = new CompositeDisposable();

    public UlkeSecFragment() {
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ImsakiyeApp.get(getActivity()).getComponent().inject(this);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_sehir_sec, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        TextView toolbarTitle = getActivity().findViewById(R.id.toolbar_title);
        toolbarTitle.setText("Ülke seçiniz");
        pd = new ProgressDialog(getActivity());
        pd.setMessage("Ülkeler getiriliyor...");
        pd.setCancelable(false);
        pd.show();
        loadUlkeler();
    }

    @OnItemClick(R.id.list_sehir)
    void onListItemClicked(int pos) {
        Ulke ulke = (Ulke) lvUlke.getItemAtPosition(pos);
        getActivity().getSupportFragmentManager().beginTransaction()
                .replace(R.id.fragment_container_sehir_sec, SehirSecFragment.newInstance(ulke))
                .addToBackStack(null)
                .commit();
    }

    private void loadUlkeler() {
        Disposable disposable = repository.getUlkeler()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::showUlkeler, this::showError);
        compositeDisposable.add(disposable);
    }

    private void showUlkeler(List<Ulke> ulkeler) {
        pd.dismiss();
        UlkeAdapter adapter = new UlkeAdapter(getActivity(), ulkeler);
        lvUlke.setAdapter(adapter);
    }

    private void showError(Throwable throwable) {
        pd.dismiss();
        Timber.e(throwable);
        Log.e("UlkeSecFragment", throwable.toString());
        Toast.makeText(getActivity(), "Ülkeler getirilirken hata oluştu.", Toast.LENGTH_SHORT)
                .show();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
        compositeDisposable.clear();
    }
}
