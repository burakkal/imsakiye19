package com.burakkal.imsakiye16.ui.aylikgorunum;

import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.widget.TextView;

import com.burakkal.imsakiye16.ImsakiyeApp;
import com.burakkal.imsakiye16.R;
import com.burakkal.imsakiye16.data.ImsakiyeRepository;
import com.burakkal.imsakiye16.data.local.entity.TblNamazVakti;
import com.burakkal.imsakiye16.ui.main.HaftalikListAdapter;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import timber.log.Timber;

public class AylikGorunumActivity extends AppCompatActivity implements AylikGorunumContract.View {

    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.toolbar_title) TextView toolbarTitle;
    @BindView(R.id.recycler_aylik_vakit) RecyclerView rvAylikVakit;
    @BindView(R.id.ad_view_aylik_gorunum) AdView adView;

    @Inject ImsakiyeRepository repository;
    private AylikGorunumPresenter presenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_aylik_gorunum);
        ButterKnife.bind(this);
        ImsakiyeApp.get(this).getComponent().inject(this);
        setSupportActionBar(toolbar);
        toolbarTitle.setText("Sakarya");
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        showAdView();
        presenter = new AylikGorunumPresenter(this, repository);
        presenter.start();
    }

    private void showAdView() {
        AdRequest adRequest = new AdRequest.Builder()
                .addTestDevice("D1A78AD6A96DF2CD6349370091094498")
                .build();
        adView.loadAd(adRequest);
    }

    @Override
    public void showTitle(String ilceAdi) {
        toolbarTitle.setText(ilceAdi);
    }

    @Override
    public void showAylikVakit(List<TblNamazVakti> vakitler) {
        HaftalikListAdapter adapter = new HaftalikListAdapter(vakitler);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        rvAylikVakit.setLayoutManager(layoutManager);
        rvAylikVakit.setAdapter(adapter);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.stop();
    }
}
