package com.burakkal.imsakiye16.ui.alarm

import android.media.MediaPlayer
import android.net.Uri
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.burakkal.imsakiye16.ImsakiyeApp
import com.burakkal.imsakiye16.R
import com.burakkal.imsakiye16.data.ImsakiyeRepository
import com.burakkal.imsakiye16.data.pref.hatirlatici.HatirlaticiPreferences
import com.burakkal.imsakiye16.ui.ayarlar.VakitHatirlatici
import kotlinx.android.synthetic.main.activity_alarm.*
import kotlinx.android.synthetic.main.layout_hadis.*
import javax.inject.Inject

class AlarmActivity: AppCompatActivity() {

    @Inject lateinit var hatirlaticiPref: HatirlaticiPreferences
    @Inject lateinit var alarmHelper: AlarmHelper
    private var mediaPlayer: MediaPlayer? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_alarm)
        ImsakiyeApp.get(this).component.inject(this)

        val vakitType = intent.getIntExtra("VakitHatirlaticiRequestCode", -1)
        when(vakitType) {
            VakitHatirlatici.SAHUR_UYANDIRMA.code -> showSahurUyandirmaUI()
            VakitHatirlatici.IMSAK_UYARI.code -> showImsakUyariUI()
            VakitHatirlatici.IFTAR_UYARI.code -> showIftarUyariUI()
        }

        showHadis()
        // COMPLETED: alarm çaldıktan sonra yeni alarmı ayarla
        // COMPLETED: sahur uyandırma alarmını tekrarlayan yap
        // COMPLETED: Hadis göster
        // TODO: Widget ayarla
        // COMPLETED: Boot receiver ekle
        // TODO: wakelock ayarlayıp ekranı aç
    }

    private fun showHadis() {
        val hadisler = resources.getStringArray(R.array.hadisler)
        tv_hadis.text = hadisler[(0 until hadisler.size).random()]
    }

    private fun showSahurUyandirmaUI() {
        tv_alarm_info.text = "Sahur Uyandırma"
        img_alarm_vakit.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_imsak))
        playSound(hatirlaticiPref.sahurUyandirmaSesi, true)
        alarmHelper.setAlarmSahurUyandirma()
    }

    private fun showImsakUyariUI() {
        tv_alarm_info.text = "İmsak Vakti"
        img_alarm_vakit.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_imsak))
        playSound(hatirlaticiPref.imsakUyariSesi)
        alarmHelper.setAlarmImsakUyari()
    }

    private fun showIftarUyariUI() {
        tv_alarm_info.text = "İftar Vakti"
        img_alarm_vakit.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_aksam))
        playSound(hatirlaticiPref.iftarUyariSesi)
        alarmHelper.setAlarmIftarUyari()
    }

    private fun playSound(uri: Uri?, isRepeating: Boolean = false) {
        if (uri == null) return
        mediaPlayer = MediaPlayer.create(this, uri)
        mediaPlayer?.setOnCompletionListener { mp ->
            if (isRepeating) {
                mp.start()
            } else {
                mp.release()
                mediaPlayer = null
            }
        }
        mediaPlayer?.start()
    }

    override fun onDestroy() {
        releaseMediaPlayer()
        alarmHelper.unsubscribe()
        super.onDestroy()
    }

    fun onKapatBtnClick(view: View) {
        finish()
    }

    private fun releaseMediaPlayer() {
        if (mediaPlayer?.isPlaying != null && mediaPlayer?.isPlaying == true) mediaPlayer?.stop()
        mediaPlayer?.release()
        mediaPlayer = null
    }
}