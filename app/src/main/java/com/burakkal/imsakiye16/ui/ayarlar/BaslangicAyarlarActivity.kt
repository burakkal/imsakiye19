package com.burakkal.imsakiye16.ui.ayarlar

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.burakkal.imsakiye16.ImsakiyeApp
import com.burakkal.imsakiye16.R
import com.burakkal.imsakiye16.data.pref.app.AppPreferences
import com.burakkal.imsakiye16.ui.main.MainActivity
import kotlinx.android.synthetic.main.app_bar.*
import javax.inject.Inject

class BaslangicAyarlarActivity : AppCompatActivity() {

    @Inject lateinit var preferences: AppPreferences

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_baslangic_ayarlar)
        ImsakiyeApp.get(this).component.inject(this)
        setToolbar()
        supportFragmentManager.beginTransaction()
                .replace(R.id.fragment_container_baslangic_ayarlar, AyarlarFragment())
                .commit()
    }

    private fun setToolbar() {
        setSupportActionBar(toolbar)
        toolbar_title.text = getString(R.string.hatirlatici_ayarlari)
        supportActionBar?.setDisplayShowTitleEnabled(false)
    }

    fun onIleriBtnClick(view: View) {
        preferences.isCitySelected = true
        startMainActivity()
    }

    private fun startMainActivity() {
        val i = Intent(this, MainActivity::class.java)
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
        startActivity(i)
    }
}