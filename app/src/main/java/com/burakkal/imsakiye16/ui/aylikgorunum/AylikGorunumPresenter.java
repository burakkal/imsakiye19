package com.burakkal.imsakiye16.ui.aylikgorunum;

import com.burakkal.imsakiye16.data.ImsakiyeRepository;
import com.burakkal.imsakiye16.ui.base.BasePresenter;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class AylikGorunumPresenter extends BasePresenter<AylikGorunumContract.View> {

    private ImsakiyeRepository repository;

    public AylikGorunumPresenter(AylikGorunumContract.View view, ImsakiyeRepository repository) {
        super(view);
        this.repository = repository;
    }

    @Override
    public void start() {
        loadTumVakitler();
    }

    private void loadTumVakitler() {
        Disposable disposable = repository.getKonum()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSuccess(konum -> view.showTitle(konum.getIlceAdi()))
                .observeOn(Schedulers.io())
                .flatMap(konum -> repository.getTumVakitler(konum.getIlceId()))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(vakitler -> view.showAylikVakit(vakitler));
        addDisposable(disposable);
    }
}
