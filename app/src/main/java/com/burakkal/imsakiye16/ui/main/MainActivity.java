package com.burakkal.imsakiye16.ui.main;

import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.burakkal.imsakiye16.ImsakiyeApp;
import com.burakkal.imsakiye16.R;
import com.burakkal.imsakiye16.data.ImsakiyeRepository;
import com.burakkal.imsakiye16.data.local.entity.TblBayramNamazi;
import com.burakkal.imsakiye16.data.local.entity.TblKonum;
import com.burakkal.imsakiye16.data.local.entity.TblNamazVakti;
import com.burakkal.imsakiye16.data.pref.app.AppPreferences;
import com.burakkal.imsakiye16.data.pref.hatirlatici.HatirlaticiPreferences;
import com.burakkal.imsakiye16.ui.ayarlar.AyarlarActivity;
import com.burakkal.imsakiye16.ui.ayarlar.VakitHatirlatici;
import com.burakkal.imsakiye16.ui.aylikgorunum.AylikGorunumActivity;
import com.burakkal.imsakiye16.ui.sehirsec.SehirSecActivity;
import com.burakkal.imsakiye16.util.AlarmUtilsKt;
import com.burakkal.imsakiye16.util.DateUtils;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Random;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.burakkal.imsakiye16.ui.main.MainPresenter.UpdateType.FORCE;

/**
 * Created by Burak on 23.4.2018.
 * burakkal54@gmail.com
 */
public class MainActivity extends AppCompatActivity implements MainContract.View {

    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.toolbar_title) TextView toolbarTitle;
    @BindView(R.id.pb_toolbar) ProgressBar pbToolbar;
    @BindView(R.id.tv_tarih) TextView tvTarih;
    @BindView(R.id.tv_kalan_sure_label) TextView tvKalanSureLabel;
    @BindView(R.id.tv_kalan_sure) TextView tvKalanSure;
    @BindView(R.id.tv_gunluk_imsak) TextView tvImsak;
    @BindView(R.id.tv_gunluk_aksam) TextView tvAksam;
    @BindView(R.id.tv_bayram_namazi_label) TextView tvBayramNamaziLabel;
    @BindView(R.id.tv_bayram_namazi_saati) TextView tvBayramNamaziSaati;
    @BindView(R.id.recycler_haftalik_vakit) RecyclerView rvHaftalikVakit;
    @BindView(R.id.tv_hadis) TextView tvHadis;
    @BindView(R.id.ad_view_main) AdView adView;

    @Inject ImsakiyeRepository repository;
    @Inject AppPreferences appPreferences;
    @Inject HatirlaticiPreferences hatirlaticiPreferences;
    private MainPresenter presenter;
    private CountDownTimer timerToNextVakit;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        ImsakiyeApp.get(this).getComponent().inject(this);
        setSupportActionBar(toolbar);
        toolbarTitle.setText("Konum bekleniyor");
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        showAdView();
        presenter = new MainPresenter(this, repository, appPreferences, hatirlaticiPreferences);
        presenter.start();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.action_vakit_guncelle:
                presenter.updateVakitler(FORCE);
                break;
            case R.id.action_aylik_gorunum:
                startAylikGorunumActivity();
                break;
            case R.id.action_sehir_degistir:
                startSehirSecWizardWithBackStack();
                break;
            case R.id.action_settings:
                startAyarlar();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.stop();
    }

    private void showAdView() {
        AdRequest adRequest = new AdRequest.Builder()
                .addTestDevice("D1A78AD6A96DF2CD6349370091094498")
                .build();
        adView.loadAd(adRequest);
    }

    private void startAylikGorunumActivity() {
        Intent i = new Intent(MainActivity.this, AylikGorunumActivity.class);
        startActivity(i);
    }

    @Override
    public void startSehirSecWizard() {
        Intent i = new Intent(MainActivity.this, SehirSecActivity.class);
        startActivity(i);
        finish();
    }

    private void startSehirSecWizardWithBackStack() {
        Intent i = new Intent(MainActivity.this, SehirSecActivity.class);
        startActivity(i);
    }

    private void startAyarlar() {
        Intent i = new Intent(MainActivity.this, AyarlarActivity.class);
        startActivity(i);
    }

    @Override
    public void showLoading() {
        pbToolbar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoding() {
        pbToolbar.setVisibility(View.INVISIBLE);
    }

    @Override
    public void showKonum(TblKonum konum) {
        toolbarTitle.setText(konum.getIlceAdi());
    }

    @Override
    public void showGununVakitleri(TblNamazVakti vakit) {
        String hicriTarih = vakit.getHicriTarih();
        String imsak = DateUtils.fromDate(vakit.getImsak(), DateUtils.FORMAT_HOUR);
        String aksam = DateUtils.fromDate(vakit.getAksam(), DateUtils.FORMAT_HOUR);
        tvTarih.setText(hicriTarih);
        tvImsak.setText(imsak);
        tvAksam.setText(aksam);
    }

    @Override
    public void showHaftalikVakitler(List<TblNamazVakti> vakitler) {
        HaftalikListAdapter adapter = new HaftalikListAdapter(vakitler);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        rvHaftalikVakit.setLayoutManager(layoutManager);
        rvHaftalikVakit.setAdapter(adapter);
        rvHaftalikVakit.setNestedScrollingEnabled(false);
    }

    @Override
    public void findVakit(List<TblNamazVakti> vakitler) {
        Date now = Calendar.getInstance().getTime();
        if (now.before(vakitler.get(0).getImsak())) {
            tvKalanSureLabel.setText("İmsak vaktine kalan süre");
            showKalanSure(vakitler.get(0).getImsak(), vakitler);
        } else if (now.before(vakitler.get(0).getAksam())) {
            tvKalanSureLabel.setText("İftar vaktine kalan süre");
            showKalanSure(vakitler.get(0).getAksam(), vakitler);
        } else {
            tvKalanSureLabel.setText("İmsak vaktine kalan süre");
            showKalanSure(vakitler.get(1).getImsak(), vakitler);
        }
    }

    public void showKalanSure(Date vakit, List<TblNamazVakti> vakitler) {
        long toNextVakit = vakit.getTime() - System.currentTimeMillis();
        if (timerToNextVakit != null) timerToNextVakit.cancel();
        timerToNextVakit = new CountDownTimer(toNextVakit, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                tvKalanSure.setText(DateUtils.fromElapsedTime(millisUntilFinished));
            }

            @Override
            public void onFinish() {
                findVakit(vakitler);
            }
        }.start();
    }

    @Override
    public void showBayramNamazi(TblBayramNamazi bayramNamazi) {
        String ramazanSaati = bayramNamazi.getRamazanSaati();
        tvBayramNamaziSaati.setText(ramazanSaati.substring(0, ramazanSaati.lastIndexOf(":")));
    }

    @Override
    public void showHadis() {
        String[] hadisler = getResources().getStringArray(R.array.hadisler);
        Random random = new Random();
        int randomValue = random.nextInt(hadisler.length);
        tvHadis.setText(hadisler[randomValue]);
    }

    @Override
    public void showError(String msg) {
        Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void setAlarm(Date date, Date nextDate, VakitHatirlatici vakitHatirlatici) {
        AlarmUtilsKt.alarmAyarla(this, vakitHatirlatici, date, nextDate);
    }

}
