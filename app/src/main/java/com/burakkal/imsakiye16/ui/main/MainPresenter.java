package com.burakkal.imsakiye16.ui.main;

import androidx.room.EmptyResultSetException;

import com.burakkal.imsakiye16.data.ImsakiyeRepository;
import com.burakkal.imsakiye16.data.local.entity.TblKonum;
import com.burakkal.imsakiye16.data.local.entity.TblNamazVakti;
import com.burakkal.imsakiye16.data.pref.app.AppPreferences;
import com.burakkal.imsakiye16.data.pref.hatirlatici.HatirlaticiPreferences;
import com.burakkal.imsakiye16.ui.ayarlar.VakitHatirlatici;
import com.burakkal.imsakiye16.ui.base.BasePresenter;
import com.burakkal.imsakiye16.util.AlarmUtilsKt;
import com.burakkal.imsakiye16.util.DateUtils;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import timber.log.Timber;

/**
 * Created by Burak on 27.4.2018.
 * burakkal54@gmail.com
 */
public class MainPresenter extends BasePresenter<MainContract.View> implements
        MainContract.Presenter {

    private ImsakiyeRepository repository;
    private AppPreferences appPreferences;
    private HatirlaticiPreferences hatirlaticiPreferences;

    enum UpdateType { SILENT, FORCE }

    MainPresenter(MainContract.View view, ImsakiyeRepository repository,
                  AppPreferences appPreferences, HatirlaticiPreferences hatirlaticiPreferences) {
        super(view);
        this.repository = repository;
        this.appPreferences = appPreferences;
        this.hatirlaticiPreferences = hatirlaticiPreferences;
    }

    @Override
    public void start() {
        if (appPreferences.isCitySelected()) {
            view.showLoading();
            loadVakitler();
            loadBayramNamazi();
            view.showHadis();
        } else {
            view.startSehirSecWizard();
        }
    }

    private void loadVakitler() {
        loadVakitler(false);
    }

    private void loadVakitler(boolean forceUpdate) {
        Disposable disposable = repository.getKonum()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSuccess(view::showKonum)
                .doOnError(this::displayKonumError)
                .observeOn(Schedulers.io())
                .flatMap(konum -> repository.getAylikVakitler(konum.getIlceId(), DateUtils.getToday(), forceUpdate))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(vakitler -> {
                    if (vakitler.size() < 14) updateVakitler(UpdateType.SILENT);
                    view.showGununVakitleri(vakitler.get(0));
                    prepareAlarms(vakitler);
                    int toIndex = vakitler.size() < 7 ? vakitler.size() : 7;
                    view.showHaftalikVakitler(vakitler.subList(0, toIndex));
                    view.findVakit(vakitler);
                    view.hideLoding();
                }, this::displayVakitlerError);
        addDisposable(disposable);
    }

    private void prepareAlarms(List<TblNamazVakti> aylikVakitler) {
        if (aylikVakitler.size() < 2) {
            updateVakitler(UpdateType.FORCE);
            return;
        }
        Date sahurUyandirma = getSahurUyandirmaDate(aylikVakitler.get(0).getImsak());
        Date imsak = aylikVakitler.get(0).getImsak();
        Date aksam = aylikVakitler.get(0).getAksam();
        Date sonrakiSahurUyandirma = getSahurUyandirmaDate(aylikVakitler.get(1).getImsak());
        Date sonrakiImsak = aylikVakitler.get(1).getImsak();
        Date sonrakiAksam = aylikVakitler.get(1).getAksam();
        if (hatirlaticiPreferences.isSahurUyandirmaChecked())
            view.setAlarm(sahurUyandirma, sonrakiSahurUyandirma, VakitHatirlatici.SAHUR_UYANDIRMA);
        if (hatirlaticiPreferences.isImsakUyariChecked())
            view.setAlarm(imsak, sonrakiImsak, VakitHatirlatici.IMSAK_UYARI);
        if (hatirlaticiPreferences.isIftarUyariChecked())
            view.setAlarm(aksam, sonrakiAksam, VakitHatirlatici.IFTAR_UYARI);
    }

    private Date getSahurUyandirmaDate(Date imsak) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(imsak);
        calendar.add(Calendar.MINUTE, hatirlaticiPreferences.getSahurUyandirmaSuresi() * -1);
        return calendar.getTime();
    }

    private void loadBayramNamazi() {
        loadBayramNamazi(false);
    }

    private void loadBayramNamazi(boolean forceUpdate) {
        Disposable disposable = repository.getKonum()
                .map(TblKonum::getIlceId)
                .flatMap(ilceId -> repository.getBayramNamazi(ilceId, forceUpdate))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(view::showBayramNamazi, this::displayBayramNamaziError);
        addDisposable(disposable);
    }

    @Override
    public void updateVakitler(UpdateType type) {
        view.showLoading();
        if (type == UpdateType.SILENT) {
            updateVakitler();
        } else if (type == UpdateType.FORCE) {
            loadVakitler(true);
            loadBayramNamazi(true);
        }
    }

    private void updateVakitler() {
        Disposable disposable = repository.getKonum()
                .subscribeOn(Schedulers.io())
                .flatMapCompletable(konum -> repository.updateVakitler(konum.getIlceId()))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::displaySuccessfullyUpdateMsg, this::displayUpdateError);
        addDisposable(disposable);
    }

    private void displayError(Throwable e) {
        view.hideLoding();
        Timber.e(e);
    }

    private void displayVakitlerError(Throwable e) {
        if (e instanceof EmptyResultSetException) {
            return;
        }
        displayError(e);
        view.showError("Vakitler getirilirken hata oluştu");
    }

    private void displayKonumError(Throwable e) {
        view.startSehirSecWizard();
    }

    private void displayBayramNamaziError(Throwable e) {
        if (e instanceof EmptyResultSetException) {
            return;
        }
        displayError(e);
        view.showError("Bayram namazı vakti getirilirken hata oluştu");
    }

    private void displayUpdateError(Throwable e) {
        displayError(e);
        view.showError("Vakitler güncellenirken hata oluştu");
    }

    private void displaySuccessfullyUpdateMsg() {
        view.hideLoding();
        Timber.i("Vakitler başarıyla güncellendi.");
    }


}
