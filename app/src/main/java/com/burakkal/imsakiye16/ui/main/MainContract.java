package com.burakkal.imsakiye16.ui.main;

import com.burakkal.imsakiye16.data.local.entity.TblBayramNamazi;
import com.burakkal.imsakiye16.data.local.entity.TblKonum;
import com.burakkal.imsakiye16.data.local.entity.TblNamazVakti;
import com.burakkal.imsakiye16.ui.ayarlar.VakitHatirlatici;

import java.util.Date;
import java.util.List;

/**
 * Created by Burak on 27.4.2018.
 * burakkal54@gmail.com
 */
public interface MainContract {

    interface View {
        void startSehirSecWizard();

        void showLoading();

        void hideLoding();

        void showKonum(TblKonum konum);

        void showGununVakitleri(TblNamazVakti vakit);

        void showHaftalikVakitler(List<TblNamazVakti> vakitler);

        void findVakit(List<TblNamazVakti> vakitler);

        void showBayramNamazi(TblBayramNamazi bayramNamazi);

        void showHadis();

        void showError(String msg);

        void setAlarm(Date date, Date nextDate, VakitHatirlatici vakitHatirlatici);
    }

    interface Presenter {
        void updateVakitler(MainPresenter.UpdateType type);
    }
}
