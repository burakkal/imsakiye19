package com.burakkal.imsakiye16.ui.aylikgorunum;

import com.burakkal.imsakiye16.data.local.entity.TblNamazVakti;

import java.util.List;

public interface AylikGorunumContract {

    interface View {
        void showTitle(String ilceAdi);

        void showAylikVakit(List<TblNamazVakti> vakitler);
    }

    interface Presenter {

    }

}
