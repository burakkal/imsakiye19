package com.burakkal.imsakiye16.ui.base;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;

/**
 * Created by Burak on 3.5.2018.
 * burakkal54@gmail.com
 */
public abstract class BasePresenter<V> {

    protected V view;
    private CompositeDisposable disposables = new CompositeDisposable();

    public BasePresenter(V view) {
        this.view = view;
    }

    public abstract void start();

    public void stop() {
        disposables.clear();
    }

    protected void addDisposable(Disposable disposable) {
        disposables.add(disposable);
    }
}
