package com.burakkal.imsakiye16.ui.sehirsec.ilce;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.burakkal.imsakiye16.ImsakiyeApp;
import com.burakkal.imsakiye16.R;
import com.burakkal.imsakiye16.data.ImsakiyeRepository;
import com.burakkal.imsakiye16.data.pref.app.AppPreferences;
import com.burakkal.imsakiye16.data.remote.model.ilceler.Ilce;
import com.burakkal.imsakiye16.data.remote.model.sehirler.Sehir;
import com.burakkal.imsakiye16.data.remote.model.ulkeler.Ulke;
import com.burakkal.imsakiye16.ui.ayarlar.BaslangicAyarlarActivity;
import com.burakkal.imsakiye16.ui.main.MainActivity;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnItemClick;
import butterknife.Unbinder;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import timber.log.Timber;

/**
 * Created by Burak on 7.5.2018.
 * burakkal54@gmail.com
 */
public class IlceSecFragment extends Fragment {

    public static final String ULKE = "ulke";
    public static final String SEHIR = "sehir";

    @BindView(R.id.list_sehir) ListView lvIlce;
    @Inject ImsakiyeRepository repository;
    @Inject AppPreferences preferences;

    private Unbinder unbinder;
    private ProgressDialog pd;
    private CompositeDisposable compositeDisposable = new CompositeDisposable();
    private Ulke ulke;
    private Sehir sehir;

    public IlceSecFragment() {
    }

    public static IlceSecFragment newInstance(Ulke ulke, Sehir sehir) {
        IlceSecFragment fragment = new IlceSecFragment();
        Bundle args = new Bundle();
        args.putSerializable(ULKE, ulke);
        args.putSerializable(SEHIR, sehir);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ImsakiyeApp.get(getActivity()).getComponent().inject(this);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_sehir_sec, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        TextView toolbarTitle = getActivity().findViewById(R.id.toolbar_title);
        toolbarTitle.setText("İlçe seçiniz");
        ulke = (Ulke) getArguments().getSerializable(ULKE);
        sehir = (Sehir) getArguments().getSerializable(SEHIR);
        pd = new ProgressDialog(getActivity());
        pd.setMessage("İlçeler getiriliyor...");
        pd.setCancelable(false);
        pd.show();
        loadIlceler(sehir.getSehirId());
    }

    @OnItemClick(R.id.list_sehir)
    void onListItemClicked(int pos) {
        Ilce ilce = (Ilce) lvIlce.getItemAtPosition(pos);
        Disposable disposable = repository.saveKonum(ulke, sehir, ilce)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(() -> {
                    boolean isCitySelected = preferences.isCitySelected();
                    if (isCitySelected) {
                        preferences.setCitySelected(true);
                        startMainActivity();
                    } else {
                        startBaslangicAyarlarActivity();
                    }
                }, e -> showError(e, "Konum bilgileri veritabanına yazılırken hata oluştu."));
        compositeDisposable.add(disposable);
    }

    private void startMainActivity() {
        Intent i = new Intent(getActivity(), MainActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(i);
        getActivity().finish();
    }

    private void startBaslangicAyarlarActivity() {
        Intent i = new Intent(getActivity(), BaslangicAyarlarActivity.class);
        startActivity(i);
    }

    private void loadIlceler(int sehirId) {
        Disposable disposable = repository.getIlceler(sehirId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::showIlceler, this::showError);
        compositeDisposable.add(disposable);
    }

    private void showIlceler(List<Ilce> ilceler) {
        pd.dismiss();
        IlceAdapter adapter = new IlceAdapter(getActivity(), ilceler);
        lvIlce.setAdapter(adapter);
    }

    private void showError(Throwable throwable) {
        showError(throwable, "Ülkeler getirilirken hata oluştu.");
    }

    private void showError(Throwable throwable, String errorMsg) {
        pd.dismiss();
        Timber.e(throwable);
        Toast.makeText(getActivity(), errorMsg, Toast.LENGTH_SHORT)
                .show();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
        compositeDisposable.clear();
    }
}
