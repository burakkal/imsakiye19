package com.burakkal.imsakiye16.ui.sehirsec.ilce;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.burakkal.imsakiye16.R;
import com.burakkal.imsakiye16.data.remote.model.ilceler.Ilce;

import java.util.List;

/**
 * Created by Burak on 7.5.2018.
 * burakkal54@gmail.com
 */
public class IlceAdapter extends ArrayAdapter<Ilce> {

    private class ViewHolder {
        TextView name;
    }

    public IlceAdapter(Context context, List<Ilce> ilceler) {
        super(context, R.layout.simple_list_item, ilceler);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Ilce ilce = getItem(position);

        ViewHolder viewHolder;

        if(convertView == null) {

            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.simple_list_item, parent, false);

            viewHolder.name = convertView.findViewById(R.id.text_row);

            convertView.setTag(viewHolder);

        } else {

            viewHolder = (ViewHolder) convertView.getTag();

        }

        viewHolder.name.setText(ilce.getIlceAdi());

        return convertView;
    }
}
