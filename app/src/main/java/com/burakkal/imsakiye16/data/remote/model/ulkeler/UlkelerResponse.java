package com.burakkal.imsakiye16.data.remote.model.ulkeler;

import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Path;
import org.simpleframework.xml.Root;

import java.util.List;

/**
 * Created by Burak on 25.4.2018.
 * burakkal54@gmail.com
 */
@Root(name = "Envelope", strict = false)
public class UlkelerResponse {

    @ElementList(name = "UlkelerResult")
    @Path(value = "Body/UlkelerResponse")
    private List<Ulke> ulkeler;

    public List<Ulke> getUlkeler() {
        return ulkeler;
    }

    public void setUlkeler(List<Ulke> ulkeler) {
        this.ulkeler = ulkeler;
    }
}
