package com.burakkal.imsakiye16.data.pref.hatirlatici;

import android.content.Context;
import android.net.Uri;

import com.burakkal.imsakiye16.R;
import com.burakkal.imsakiye16.data.pref.base.BasePreferences;
import com.burakkal.imsakiye16.util.AppUtilsKt;

import androidx.annotation.StringRes;

public class HatirlaticiPreferences extends BasePreferences
        implements HatirlaticiPreferencesDataSource {

    public HatirlaticiPreferences(Context context) {
        super(context);
    }

    @Override
    public boolean isSahurUyandirmaChecked() {
        return getBoolean(R.string.pref_sahur_uyandirma_key, true);
    }

    @Override
    public boolean isImsakUyariChecked() {
        return getBoolean(R.string.pref_imsak_uyari_key, true);
    }

    @Override
    public boolean isIftarUyariChecked() {
        return getBoolean(R.string.pref_iftar_uyari_key, true);
    }

    @Override
    public Uri getSahurUyandirmaSesi() {
        return Uri.parse(getString(R.string.pref_sahur_uyandirma_sesi_key,
                AppUtilsKt.getRawUri(R.raw.ramazan_davulu_mani, context).toString()));
    }

    @Override
    public Uri getSahurUyandirmaSesi(String defValue) {
        return Uri.parse(getString(R.string.pref_sahur_uyandirma_sesi_key, defValue));
    }

    @Override
    public void setSahurUyandirmaSesi(String value) {
        setString(R.string.pref_sahur_uyandirma_sesi_key, value);
    }

    @Override
    public String getSahurUyandirmaSesiAdi() {
        return getString(R.string.pref_sahur_uyandirma_sesi_name_key, R.string.pref_sahur_uyandirma_sesi_name_default);
    }

    @Override
    public String getSahurUyandirmaSesiAdi(@StringRes int defValue) {
        return getString(R.string.pref_sahur_uyandirma_sesi_name_key, defValue);
    }

    @Override
    public void setSahurUyandirmaSesiAdi(String value) {
        setString(R.string.pref_sahur_uyandirma_sesi_name_key, value);
    }

    @Override
    public Uri getImsakUyariSesi() {
        return Uri.parse(getString(R.string.pref_imsak_uyari_sesi_key,
                AppUtilsKt.getRawUri(R.raw.makkah_azan, context).toString()));
    }

    @Override
    public Uri getImsakUyariSesi(String defValue) {
        return Uri.parse(getString(R.string.pref_imsak_uyari_sesi_key, defValue));
    }

    @Override
    public void setImsakUyariSesi(String value) {
        setString(R.string.pref_imsak_uyari_sesi_key, value);
    }

    @Override
    public String getImsakUyariSesiAdi() {
        return getString(R.string.pref_imsak_uyari_sesi_name_key, R.string.pref_imsak_uyari_sesi_name_default);
    }

    @Override
    public String getImsakUyariSesiAdi(@StringRes int defValue) {
        return getString(R.string.pref_imsak_uyari_sesi_name_key, defValue);
    }

    @Override
    public void setImsakUyariSesiAdi(String value) {
        setString(R.string.pref_imsak_uyari_sesi_name_key, value);
    }

    @Override
    public Uri getIftarUyariSesi() {
        return Uri.parse(getString(R.string.pref_iftar_uyari_sesi_key,
                AppUtilsKt.getRawUri(R.raw.makkah_azan, context).toString()));
    }

    @Override
    public Uri getIftarUyariSesi(String defValue) {
        return Uri.parse(getString(R.string.pref_iftar_uyari_sesi_key, defValue));
    }

    @Override
    public void setIftarUyariSesi(String value) {
        setString(R.string.pref_iftar_uyari_sesi_key, value);
    }

    @Override
    public String getIftarUyariSesiAdi() {
        return getString(R.string.pref_iftar_uyari_sesi_name_key, R.string.pref_iftar_uyari_sesi_name_default);
    }

    @Override
    public String getIftarUyariSesiAdi(@StringRes int defValue) {
        return getString(R.string.pref_iftar_uyari_sesi_name_key, defValue);
    }

    @Override
    public void setIftarUyariSesiAdi(String value) {
        setString(R.string.pref_iftar_uyari_sesi_name_key, value);
    }

    @Override
    public int getSahurUyandirmaSuresi() {
        try {
            return Integer.parseInt(getString(R.string.pref_sahur_uyandirma_suresi_key, "60"));
        } catch (NumberFormatException e) {
            return 60;
        }
    }
}
