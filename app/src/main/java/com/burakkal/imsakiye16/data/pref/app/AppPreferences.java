package com.burakkal.imsakiye16.data.pref.app;

import android.content.Context;

import com.burakkal.imsakiye16.R;
import com.burakkal.imsakiye16.data.pref.base.BasePreferences;

/**
 * Created by Burak on 27.4.2018.
 * burakkal54@gmail.com
 */
public class AppPreferences extends BasePreferences implements AppPreferencesDataSource {

    public AppPreferences(Context context) {
        super(context);
    }

    @Override
    public boolean isCitySelected() {
        return getBoolean(R.string.city_selected_key, false);
    }

    @Override
    public void setCitySelected(boolean value) {
        setBoolean(R.string.city_selected_key, value);
    }
}
