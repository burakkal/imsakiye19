package com.burakkal.imsakiye16.data.local;

import androidx.room.Database;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;

import com.burakkal.imsakiye16.data.local.converter.DateToLongConverter;
import com.burakkal.imsakiye16.data.local.dao.BayramNamaziDao;
import com.burakkal.imsakiye16.data.local.dao.ImsakiyeDao;
import com.burakkal.imsakiye16.data.local.dao.KonumDao;
import com.burakkal.imsakiye16.data.local.dao.NamazVaktiDao;
import com.burakkal.imsakiye16.data.local.entity.TblBayramNamazi;
import com.burakkal.imsakiye16.data.local.entity.TblImsakiye;
import com.burakkal.imsakiye16.data.local.entity.TblKonum;
import com.burakkal.imsakiye16.data.local.entity.TblNamazVakti;

/**
 * Created by Burak on 9.5.2018.
 * burakkal54@gmail.com
 */
@Database(entities = {TblKonum.class, TblNamazVakti.class,
        TblBayramNamazi.class, TblImsakiye.class}, version = 2)
@TypeConverters({DateToLongConverter.class})
public abstract class ImsakiyeDatabase extends RoomDatabase {

    public abstract KonumDao konumDao();
    public abstract NamazVaktiDao namazVaktiDao();
    public abstract BayramNamaziDao bayramNamaziDao();
    public abstract ImsakiyeDao imsakiyeDao();
}
