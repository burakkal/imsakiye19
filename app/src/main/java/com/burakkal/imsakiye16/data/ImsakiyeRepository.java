package com.burakkal.imsakiye16.data;

import com.burakkal.imsakiye16.BuildConfig;
import com.burakkal.imsakiye16.data.local.ImsakiyeDatabase;
import com.burakkal.imsakiye16.data.local.entity.TblBayramNamazi;
import com.burakkal.imsakiye16.data.local.entity.TblKonum;
import com.burakkal.imsakiye16.data.local.entity.TblNamazVakti;
import com.burakkal.imsakiye16.data.remote.DiyanetApi;
import com.burakkal.imsakiye16.data.remote.model.ilceler.Ilce;
import com.burakkal.imsakiye16.data.remote.model.ilceler.IlceDetayRequest;
import com.burakkal.imsakiye16.data.remote.model.ilceler.IlceDetayResponse;
import com.burakkal.imsakiye16.data.remote.model.ilceler.IlcelerRequest;
import com.burakkal.imsakiye16.data.remote.model.ilceler.IlcelerResponse;
import com.burakkal.imsakiye16.data.remote.model.namazvakti.aylik.AylikNamazVaktiRequest;
import com.burakkal.imsakiye16.data.remote.model.namazvakti.aylik.AylikNamazVaktiResponse;
import com.burakkal.imsakiye16.data.remote.model.namazvakti.aylik.NamazVakti;
import com.burakkal.imsakiye16.data.remote.model.namazvakti.bayram.BayramNamaziRequest;
import com.burakkal.imsakiye16.data.remote.model.sehirler.Sehir;
import com.burakkal.imsakiye16.data.remote.model.sehirler.SehirlerRequest;
import com.burakkal.imsakiye16.data.remote.model.sehirler.SehirlerResponse;
import com.burakkal.imsakiye16.data.remote.model.ulkeler.Ulke;
import com.burakkal.imsakiye16.data.remote.model.ulkeler.UlkelerRequest;
import com.burakkal.imsakiye16.data.remote.model.ulkeler.UlkelerResponse;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.Single;
import timber.log.Timber;

/**
 * Created by Burak on 26.4.2018.
 * burakkal54@gmail.com
 */
public class ImsakiyeRepository implements ImsakiyeDataSource {

    private DiyanetApi diyanetApi;
    private ImsakiyeDatabase imsakiyeDb;

    private List<Ulke> cacheUlkeler = new ArrayList<>();
    private Map<Integer, List<Sehir>> cacheSehirler = new HashMap<>();
    private Map<Integer, List<Ilce>> cacheIlceler = new HashMap<>();

    public ImsakiyeRepository(DiyanetApi diyanetApi, ImsakiyeDatabase imsakiyeDb) {
        this.diyanetApi = diyanetApi;
        this.imsakiyeDb = imsakiyeDb;
    }

    @Override
    public Single<List<Ulke>> getUlkeler() {
        if (cacheUlkeler.size() > 0) {
            return Single.just(cacheUlkeler);
        }
        UlkelerRequest ulkelerRequest =
                new UlkelerRequest(BuildConfig.DIYANET_USERNAME, BuildConfig.DIYANET_PASSWORD);
        return diyanetApi.getUlkeler(ulkelerRequest)
                .map(UlkelerResponse::getUlkeler)
                .doOnSuccess(ulkeler -> cacheUlkeler.addAll(ulkeler));
    }

    @Override
    public Single<List<Sehir>> getSehirler(int ulkeId) {
        if (cacheSehirler.containsKey(ulkeId)) {
            return Single.just(cacheSehirler.get(ulkeId));
        }
        SehirlerRequest sehirlerRequest = new SehirlerRequest(
                ulkeId, BuildConfig.DIYANET_USERNAME, BuildConfig.DIYANET_PASSWORD);
        return diyanetApi.getSehirler(sehirlerRequest)
                .map(SehirlerResponse::getSehirler)
                .doOnSuccess(sehirler -> cacheSehirler.put(ulkeId, sehirler));
    }

    @Override
    public Single<List<Ilce>> getIlceler(int sehirId) {
        if (cacheIlceler.containsKey(sehirId)) {
            return Single.just(cacheIlceler.get(sehirId));
        }
        IlcelerRequest ilcelerRequest = new IlcelerRequest(
                sehirId, BuildConfig.DIYANET_USERNAME, BuildConfig.DIYANET_PASSWORD);
        return diyanetApi.getIlceler(ilcelerRequest)
                .map(IlcelerResponse::getIlceler)
                .doOnSuccess(ilceler -> cacheIlceler.put(sehirId, ilceler));
    }

    @Override
    public Completable saveKonum(Ulke ulke, Sehir sehir, Ilce ilce) {
        TblKonum konum = new TblKonum(
                ilce.getIlceId(), ilce.getIlceAdi(), ilce.getIlceAdiEn(), sehir, ulke);
        Single<Integer> maxSortNum = imsakiyeDb.konumDao().getMaxSortNum()
                .toSingle(0);
        Single<IlceDetayResponse> ilceDetay = getIlceDetay(ilce.getIlceId());
        return Single
                .zip(maxSortNum, ilceDetay, (a, b) -> {
                    konum.setSortNum(a + 1);
                    konum.setCografiKibleAcisi(b.getCografiKibleAcisi());
                    konum.setKibleAcisi(b.getKibleAcisi());
                    konum.setKabeyeUzaklik(b.getKabeyeUzaklik());
                    return konum;
                })
                .flatMapCompletable(konum1 -> Completable.fromAction(() -> imsakiyeDb.konumDao().insert(konum1)));
    }

    private Single<IlceDetayResponse> getIlceDetay(int ilceId) {
        IlceDetayRequest ilceDetayRequest = new IlceDetayRequest(
                ilceId, BuildConfig.DIYANET_USERNAME, BuildConfig.DIYANET_PASSWORD);
        return diyanetApi.getIlceDetay(ilceDetayRequest);
    }

    @Override
    public Single<TblKonum> getKonum() {
        Timber.i("getKonum()");
        return imsakiyeDb.konumDao().getLastAddedKonum();
    }

    @Override
    public Single<TblNamazVakti> getGunlukVakitler(int ilceId, Date from) {
        return getAylikVakitler(ilceId, from)
                .flatMapObservable(Observable::fromIterable)
                .firstOrError();
    }

    @Override
    public Single<List<TblNamazVakti>> getHaftalikVakitler(int ilceId, Date from) {
        return getAylikVakitler(ilceId, from)
                .flatMapObservable(Observable::fromIterable)
                .take(7)
                .toList();
    }

    @Override
    public Single<List<TblNamazVakti>> getAylikVakitler(int ilceId, Date from) {
        return getAylikVakitler(ilceId, from, false);
    }

    @Override
    public Single<List<TblNamazVakti>> getAylikVakitler(int ilceId, Date from, boolean forceUpdate) {
        Timber.i("getAylikVakitler(%d, %s, %b)", ilceId, from.toString(), forceUpdate);
        Single<List<TblNamazVakti>> aylikVakitlerFromRemote = getAnSaveAylikVakitlerFromRemote(ilceId)
                .flatMapObservable(Observable::fromIterable)
                .filter(vakit -> vakit.getMiladiTarih().compareTo(from) >= 0)
                .toList();
        if (forceUpdate) return aylikVakitlerFromRemote;
        Single<List<TblNamazVakti>> aylikVakitlerFromDb =
                imsakiyeDb.namazVaktiDao().getAylikVakitler(ilceId, from);
        return Single.concat(aylikVakitlerFromDb, aylikVakitlerFromRemote)
                .filter(vakitler -> !vakitler.isEmpty())
                .firstOrError();
    }

    @Override
    public Single<List<TblNamazVakti>> getTumVakitler(int ilceId) {
        Timber.i("getTumVakitler[%d]", ilceId);
        return imsakiyeDb.namazVaktiDao().getAylikVakitler(ilceId);
    }

    @Override
    public Completable updateVakitler(int ilceId) {
        return getAnSaveAylikVakitlerFromRemote(ilceId).toCompletable();
    }

    private Single<List<TblNamazVakti>> getAnSaveAylikVakitlerFromRemote(int ilceId) {
        Timber.i("getAnSaveAylikVakitlerFromRemote(%d)", ilceId);
        AylikNamazVaktiRequest request = new AylikNamazVaktiRequest(
                ilceId, BuildConfig.DIYANET_USERNAME, BuildConfig.DIYANET_PASSWORD);
        return diyanetApi.getAylikNamazVakitleri(request)
                .map(AylikNamazVaktiResponse::getNamazVakitleri)
                .flatMap(vakitler -> {
                    imsakiyeDb.namazVaktiDao().deleteByIlceId(ilceId);
                    return saveVakitlerToDb(vakitler, ilceId);
                });
    }

    private Single<List<TblNamazVakti>> saveVakitlerToDb(List<NamazVakti> vakitler, int ilceId) {
        Timber.i("saveVakitlerToDb(%d, %d)", vakitler.size(), ilceId);
        return Observable.fromIterable(vakitler)
                .map(namazVakti -> namazVakti.toTblNamazVakti(ilceId))
                .doOnNext(tblNamazVakti -> imsakiyeDb.namazVaktiDao().insert(tblNamazVakti))
                .toList();
    }

    @Override
    public Single<TblBayramNamazi> getBayramNamazi(int ilceId, boolean forceUpdate) {
        Single<TblBayramNamazi> bayramNamaziFromRemote = getAndSaveBayramNamaziFromRemote(ilceId);
        if (forceUpdate) return bayramNamaziFromRemote;
        Single<TblBayramNamazi> bayramNamaziFromDb = imsakiyeDb.bayramNamaziDao()
                .getBayramNamaziByIlceId(ilceId);
        return bayramNamaziFromDb.onErrorResumeNext(bayramNamaziFromRemote);
    }

    private Single<TblBayramNamazi> getAndSaveBayramNamaziFromRemote(int ilceId) {
        BayramNamaziRequest request = new BayramNamaziRequest(
                ilceId, BuildConfig.DIYANET_USERNAME, BuildConfig.DIYANET_PASSWORD);
        return diyanetApi.getBayramNamazi(request)
                .map(bayramNamaziResponse -> bayramNamaziResponse.toTblBayramNamazi(ilceId))
                .doOnSuccess(tblBayramNamazi -> imsakiyeDb.bayramNamaziDao().insert(tblBayramNamazi));
    }

    @Override
    public Single<TblNamazVakti> getGunlukVakitlerByIlceId(Date date, int ilceId) {
        return imsakiyeDb.namazVaktiDao().getGunlukVakitlerByIlceId(date, ilceId);
    }
}
