package com.burakkal.imsakiye16.data.local.entity;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.PrimaryKey;

import java.util.Date;

/**
 * Created by Burak on 14.5.2018.
 * burakkal54@gmail.com
 */
@Entity(primaryKeys = {"ilceId", "miladiTarih"},
        foreignKeys = @ForeignKey(entity = TblKonum.class,
                parentColumns = "ilceId",
                childColumns = "ilceId",
                onDelete = ForeignKey.CASCADE))
public class TblNamazVakti {

    @NonNull private Date miladiTarih;
    private Date imsak;
    private Date gunes;
    private Date ogle;
    private Date ikindi;
    private Date aksam;
    private Date yatsi;
    private Date gunesBatis;
    private Date gunesDogus;
    private Date kibleSaati;
    private String hicriTarih;
    private String ayinSekliUrl;
    private int ilceId;

    public Date getMiladiTarih() {
        return miladiTarih;
    }

    public void setMiladiTarih(Date miladiTarih) {
        this.miladiTarih = miladiTarih;
    }

    public Date getImsak() {
        return imsak;
    }

    public void setImsak(Date imsak) {
        this.imsak = imsak;
    }

    public Date getGunes() {
        return gunes;
    }

    public void setGunes(Date gunes) {
        this.gunes = gunes;
    }

    public Date getOgle() {
        return ogle;
    }

    public void setOgle(Date ogle) {
        this.ogle = ogle;
    }

    public Date getIkindi() {
        return ikindi;
    }

    public void setIkindi(Date ikindi) {
        this.ikindi = ikindi;
    }

    public Date getAksam() {
        return aksam;
    }

    public void setAksam(Date aksam) {
        this.aksam = aksam;
    }

    public Date getYatsi() {
        return yatsi;
    }

    public void setYatsi(Date yatsi) {
        this.yatsi = yatsi;
    }

    public Date getGunesBatis() {
        return gunesBatis;
    }

    public void setGunesBatis(Date gunesBatis) {
        this.gunesBatis = gunesBatis;
    }

    public Date getGunesDogus() {
        return gunesDogus;
    }

    public void setGunesDogus(Date gunesDogus) {
        this.gunesDogus = gunesDogus;
    }

    public Date getKibleSaati() {
        return kibleSaati;
    }

    public void setKibleSaati(Date kibleSaati) {
        this.kibleSaati = kibleSaati;
    }

    public String getHicriTarih() {
        return hicriTarih;
    }

    public void setHicriTarih(String hicriTarih) {
        this.hicriTarih = hicriTarih;
    }

    public String getAyinSekliUrl() {
        return ayinSekliUrl;
    }

    public void setAyinSekliUrl(String ayinSekliUrl) {
        this.ayinSekliUrl = ayinSekliUrl;
    }

    public int getIlceId() {
        return ilceId;
    }

    public void setIlceId(int ilceId) {
        this.ilceId = ilceId;
    }
}
