package com.burakkal.imsakiye16.data.local.entity;

import androidx.room.Embedded;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.burakkal.imsakiye16.data.remote.model.sehirler.Sehir;
import com.burakkal.imsakiye16.data.remote.model.ulkeler.Ulke;

/**
 * Created by Burak on 10.5.2018.
 * burakkal54@gmail.com
 */
@Entity
public class TblKonum {

    @PrimaryKey private int ilceId;
    private String ilceAdi;
    private String ilceAdiEn;
    @Embedded private Sehir sehir;
    @Embedded private Ulke ulke;
    private int sortNum;
    private int cografiKibleAcisi;
    private int kibleAcisi;
    private int kabeyeUzaklik;

    public TblKonum(int ilceId, String ilceAdi, String ilceAdiEn, Sehir sehir, Ulke ulke) {
        this.ilceId = ilceId;
        this.ilceAdi = ilceAdi;
        this.ilceAdiEn = ilceAdiEn;
        this.sehir = sehir;
        this.ulke = ulke;
    }

    public int getIlceId() {
        return ilceId;
    }

    public void setIlceId(int ilceId) {
        this.ilceId = ilceId;
    }

    public String getIlceAdi() {
        return ilceAdi;
    }

    public void setIlceAdi(String ilceAdi) {
        this.ilceAdi = ilceAdi;
    }

    public String getIlceAdiEn() {
        return ilceAdiEn;
    }

    public void setIlceAdiEn(String ilceAdiEn) {
        this.ilceAdiEn = ilceAdiEn;
    }

    public Sehir getSehir() {
        return sehir;
    }

    public void setSehir(Sehir sehir) {
        this.sehir = sehir;
    }

    public Ulke getUlke() {
        return ulke;
    }

    public void setUlke(Ulke ulke) {
        this.ulke = ulke;
    }

    public int getSortNum() {
        return sortNum;
    }

    public void setSortNum(int sortNum) {
        this.sortNum = sortNum;
    }

    public int getCografiKibleAcisi() {
        return cografiKibleAcisi;
    }

    public void setCografiKibleAcisi(int cografiKibleAcisi) {
        this.cografiKibleAcisi = cografiKibleAcisi;
    }

    public int getKibleAcisi() {
        return kibleAcisi;
    }

    public void setKibleAcisi(int kibleAcisi) {
        this.kibleAcisi = kibleAcisi;
    }

    public int getKabeyeUzaklik() {
        return kabeyeUzaklik;
    }

    public void setKabeyeUzaklik(int kabeyeUzaklik) {
        this.kabeyeUzaklik = kabeyeUzaklik;
    }
}
