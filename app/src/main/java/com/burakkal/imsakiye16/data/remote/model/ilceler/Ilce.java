package com.burakkal.imsakiye16.data.remote.model.ilceler;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

import java.io.Serializable;

/**
 * Created by Burak on 25.4.2018.
 * burakkal54@gmail.com
 */
@Root(name = "Ilce")
public class Ilce implements Serializable {

    @Element(name = "IlceAdi")
    private String ilceAdi;

    @Element(name = "IlceAdiEn")
    private String ilceAdiEn;

    @Element(name = "IlceID")
    private int ilceId;

    public String getIlceAdi() {
        return ilceAdi;
    }

    public void setIlceAdi(String ilceAdi) {
        this.ilceAdi = ilceAdi;
    }

    public String getIlceAdiEn() {
        return ilceAdiEn;
    }

    public void setIlceAdiEn(String ilceAdiEn) {
        this.ilceAdiEn = ilceAdiEn;
    }

    public int getIlceId() {
        return ilceId;
    }

    public void setIlceId(int ilceId) {
        this.ilceId = ilceId;
    }
}
