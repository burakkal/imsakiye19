package com.burakkal.imsakiye16.data;

import com.burakkal.imsakiye16.data.local.entity.TblBayramNamazi;
import com.burakkal.imsakiye16.data.local.entity.TblKonum;
import com.burakkal.imsakiye16.data.local.entity.TblNamazVakti;
import com.burakkal.imsakiye16.data.remote.model.ilceler.Ilce;
import com.burakkal.imsakiye16.data.remote.model.sehirler.Sehir;
import com.burakkal.imsakiye16.data.remote.model.ulkeler.Ulke;

import java.util.Date;
import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Single;

/**
 * Created by Burak on 26.4.2018.
 * burakkal54@gmail.com
 */
public interface ImsakiyeDataSource {

    Single<List<Ulke>> getUlkeler();

    Single<List<Sehir>> getSehirler(int ulkeId);

    Single<List<Ilce>> getIlceler(int sehirId);

    Completable saveKonum(Ulke ulke, Sehir sehir, Ilce ilce);

    Single<TblKonum> getKonum();

    Single<TblNamazVakti> getGunlukVakitler(int ilceId, Date from);

    Single<List<TblNamazVakti>> getHaftalikVakitler(int ilceId, Date from);

    Single<List<TblNamazVakti>> getAylikVakitler(int ilceId, Date from);

    Single<List<TblNamazVakti>> getAylikVakitler(int ilceId, Date from, boolean forceUpdate);

    Single<List<TblNamazVakti>> getTumVakitler(int ilceId);

    Completable updateVakitler(int ilceId);

    Single<TblBayramNamazi> getBayramNamazi(int ilceId, boolean forceUpdate);

    Single<TblNamazVakti> getGunlukVakitlerByIlceId(Date date, int ilceId);
}
