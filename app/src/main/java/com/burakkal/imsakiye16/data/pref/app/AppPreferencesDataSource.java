package com.burakkal.imsakiye16.data.pref.app;

/**
 * Created by Burak on 27.4.2018.
 * burakkal54@gmail.com
 */
public interface AppPreferencesDataSource {

    boolean isCitySelected();

    void setCitySelected(boolean value);
}
