package com.burakkal.imsakiye16.data.remote.model.ulkeler;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

import java.io.Serializable;

/**
 * Created by Burak on 25.4.2018.
 * burakkal54@gmail.com
 */
@Root(name = "Ulke")
public class Ulke implements Serializable {

    @Element(name = "UlkeAdi")
    private String ulkeAdi;

    @Element(name = "UlkeAdiEn")
    private String ulkeAdiEn;

    @Element(name = "UlkeID")
    private int ulkeId;

    public String getUlkeAdi() {
        return ulkeAdi;
    }

    public void setUlkeAdi(String ulkeAdi) {
        this.ulkeAdi = ulkeAdi;
    }

    public String getUlkeAdiEn() {
        return ulkeAdiEn;
    }

    public void setUlkeAdiEn(String ulkeAdiEn) {
        this.ulkeAdiEn = ulkeAdiEn;
    }

    public int getUlkeId() {
        return ulkeId;
    }

    public void setUlkeId(int ulkeId) {
        this.ulkeId = ulkeId;
    }
}
