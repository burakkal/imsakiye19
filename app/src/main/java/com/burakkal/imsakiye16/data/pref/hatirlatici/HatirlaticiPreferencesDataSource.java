package com.burakkal.imsakiye16.data.pref.hatirlatici;

import android.net.Uri;

import androidx.annotation.StringRes;

public interface HatirlaticiPreferencesDataSource {

    boolean isSahurUyandirmaChecked();

    boolean isImsakUyariChecked();

    boolean isIftarUyariChecked();

    Uri getSahurUyandirmaSesi();

    Uri getSahurUyandirmaSesi(String defValue);

    void setSahurUyandirmaSesi(String value);

    String getSahurUyandirmaSesiAdi();

    String getSahurUyandirmaSesiAdi(@StringRes int defValue);

    void setSahurUyandirmaSesiAdi(String value);

    Uri getImsakUyariSesi();

    Uri getImsakUyariSesi(String defValue);

    void setImsakUyariSesi(String value);

    String getImsakUyariSesiAdi();

    String getImsakUyariSesiAdi(@StringRes int defValue);

    void setImsakUyariSesiAdi(String value);

    Uri getIftarUyariSesi();

    Uri getIftarUyariSesi(String defValue);

    void setIftarUyariSesi(String value);

    String getIftarUyariSesiAdi();

    String getIftarUyariSesiAdi(@StringRes int defValue);

    void setIftarUyariSesiAdi(String value);

    int getSahurUyandirmaSuresi();
}
