package com.burakkal.imsakiye16.data.remote.model.ilceler;

import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Path;
import org.simpleframework.xml.Root;

import java.util.List;

/**
 * Created by Burak on 25.4.2018.
 * burakkal54@gmail.com
 */
@Root(name = "Envelope", strict = false)
public class IlcelerResponse {

    @ElementList(name = "IlcelerResult")
    @Path(value = "Body/IlcelerResponse")
    private List<Ilce> ilceler;

    public List<Ilce> getIlceler() {
        return ilceler;
    }

    public void setIlceler(List<Ilce> ilceler) {
        this.ilceler = ilceler;
    }
}
