package com.burakkal.imsakiye16.data.remote.model.namazvakti.bayram;

import com.burakkal.imsakiye16.data.local.entity.TblBayramNamazi;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Path;
import org.simpleframework.xml.Root;

/**
 * Created by Burak on 25.4.2018.
 * burakkal54@gmail.com
 */
@Root(name = "Envelope", strict = false)
public class BayramNamaziResponse {

    @Element(name = "RamazanBayramNamaziHTarihi")
    @Path(value = "Body/BayramNamaziVaktiResponse/BayramNamaziVaktiResult")
    private String ramazanHicriTarih;

    @Element(name = "RamazanBayramNamaziTarihi")
    @Path(value = "Body/BayramNamaziVaktiResponse/BayramNamaziVaktiResult")
    private String ramazanTarih;

    @Element(name = "RamazanBayramNamaziSaati")
    @Path(value = "Body/BayramNamaziVaktiResponse/BayramNamaziVaktiResult")
    private String ramazanSaati;

    @Element(name = "KurbanBayramNamaziHTarihi")
    @Path(value = "Body/BayramNamaziVaktiResponse/BayramNamaziVaktiResult")
    private String kurbanHicriTarih;

    @Element(name = "KurbanBayramNamaziTarihi")
    @Path(value = "Body/BayramNamaziVaktiResponse/BayramNamaziVaktiResult")
    private String kurbanTarih;

    @Element(name = "KurbanBayramNamaziSaati")
    @Path(value = "Body/BayramNamaziVaktiResponse/BayramNamaziVaktiResult")
    private String kurbanSaati;

    public String getRamazanHicriTarih() {
        return ramazanHicriTarih;
    }

    public void setRamazanHicriTarih(String ramazanHicriTarih) {
        this.ramazanHicriTarih = ramazanHicriTarih;
    }

    public String getRamazanTarih() {
        return ramazanTarih;
    }

    public void setRamazanTarih(String ramazanTarih) {
        this.ramazanTarih = ramazanTarih;
    }

    public String getRamazanSaati() {
        return ramazanSaati;
    }

    public void setRamazanSaati(String ramazanSaati) {
        this.ramazanSaati = ramazanSaati;
    }

    public String getKurbanHicriTarih() {
        return kurbanHicriTarih;
    }

    public void setKurbanHicriTarih(String kurbanHicriTarih) {
        this.kurbanHicriTarih = kurbanHicriTarih;
    }

    public String getKurbanTarih() {
        return kurbanTarih;
    }

    public void setKurbanTarih(String kurbanTarih) {
        this.kurbanTarih = kurbanTarih;
    }

    public String getKurbanSaati() {
        return kurbanSaati;
    }

    public void setKurbanSaati(String kurbanSaati) {
        this.kurbanSaati = kurbanSaati;
    }

    public TblBayramNamazi toTblBayramNamazi(int ilceId) {
        TblBayramNamazi tblBayramNamazi = new TblBayramNamazi();
        tblBayramNamazi.setIlceId(ilceId);
        tblBayramNamazi.setRamazanTarih(ramazanTarih);
        tblBayramNamazi.setRamazanHicriTarih(ramazanHicriTarih);
        tblBayramNamazi.setRamazanSaati(ramazanSaati);
        tblBayramNamazi.setKurbanTarih(kurbanTarih);
        tblBayramNamazi.setKurbanHicriTarih(kurbanHicriTarih);
        tblBayramNamazi.setKurbanSaati(kurbanSaati);
        return tblBayramNamazi;
    }
}
