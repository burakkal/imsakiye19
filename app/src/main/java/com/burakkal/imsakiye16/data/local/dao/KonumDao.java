package com.burakkal.imsakiye16.data.local.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.burakkal.imsakiye16.data.local.entity.TblKonum;

import io.reactivex.Maybe;
import io.reactivex.Single;

/**
 * Created by Burak on 10.5.2018.
 * burakkal54@gmail.com
 */
@Dao
public interface KonumDao {

    @Query("SELECT * FROM tblkonum LIMIT 1")
    Single<TblKonum> getKonum();

    @Query("SELECT * FROM tblkonum ORDER BY sortNum DESC LIMIT 1")
    Single<TblKonum> getLastAddedKonum();

    @Query("SELECT max(sortNum) FROM tblkonum")
    Maybe<Integer> getMaxSortNum();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(TblKonum... konumlar);
}
