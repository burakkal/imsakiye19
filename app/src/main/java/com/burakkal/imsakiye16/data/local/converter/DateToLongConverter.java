package com.burakkal.imsakiye16.data.local.converter;

import androidx.room.TypeConverter;

import java.util.Date;

/**
 * Created by Burak on 15.5.2018.
 * burakkal54@gmail.com
 */
public class DateToLongConverter {

    @TypeConverter
    public static Date fromTimestamp(Long value) {
        return value == null ? null : new Date(value);
    }

    @TypeConverter
    public static Long toTimestamp(Date date) {
        return date == null ? null : date.getTime();
    }
}
