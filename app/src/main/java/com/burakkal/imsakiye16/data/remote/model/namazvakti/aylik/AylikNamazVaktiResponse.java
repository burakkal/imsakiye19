package com.burakkal.imsakiye16.data.remote.model.namazvakti.aylik;

import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Path;
import org.simpleframework.xml.Root;

import java.util.List;

/**
 * Created by Burak on 25.4.2018.
 * burakkal54@gmail.com
 */
@Root(name = "Envelope", strict = false)
public class AylikNamazVaktiResponse {

    @ElementList(name = "AylikNamazVaktiResult")
    @Path(value = "Body/AylikNamazVaktiResponse")
    private List<NamazVakti> namazVakitleri;

    public List<NamazVakti> getNamazVakitleri() {
        return namazVakitleri;
    }

    public void setNamazVakitleri(List<NamazVakti> namazVakitleri) {
        this.namazVakitleri = namazVakitleri;
    }
}
