package com.burakkal.imsakiye16.data.remote.model.sehirler;

import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Path;
import org.simpleframework.xml.Root;

import java.util.List;

/**
 * Created by Burak on 25.4.2018.
 * burakkal54@gmail.com
 */
@Root(name = "Envelope", strict = false)
public class SehirlerResponse {

    @ElementList(name = "SehirlerResult")
    @Path(value = "Body/SehirlerResponse")
    private List<Sehir> sehirler;

    public List<Sehir> getSehirler() {
        return sehirler;
    }

    public void setSehirler(List<Sehir> sehirler) {
        this.sehirler = sehirler;
    }
}
