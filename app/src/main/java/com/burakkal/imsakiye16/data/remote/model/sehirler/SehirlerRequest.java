package com.burakkal.imsakiye16.data.remote.model.sehirler;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Namespace;
import org.simpleframework.xml.NamespaceList;
import org.simpleframework.xml.Order;
import org.simpleframework.xml.Path;
import org.simpleframework.xml.Root;

/**
 * Created by Burak on 25.4.2018.
 * burakkal54@gmail.com
 */
@Root(name = "s:Envelope")
@NamespaceList({
        @Namespace(reference = "http://schemas.xmlsoap.org/soap/envelope/", prefix = "s"),
        @Namespace(reference = "http://tempuri.org/", prefix = "t")
})
@Order(elements = {"s:Body/t:Sehirler[1]/UlkeID", "s:Body/t:Sehirler[1]/username",
        "s:Body/t:Sehirler[1]/password"})
public class SehirlerRequest {

    @Element(name = "UlkeID")
    @Path(value = "s:Body/t:Sehirler[1]")
    @Namespace(reference = "http://tempuri.org/")
    private int ulkeId;

    @Element
    @Path(value = "s:Body/t:Sehirler[1]")
    @Namespace(reference = "http://tempuri.org/")
    private String username;

    @Element
    @Path(value = "s:Body/t:Sehirler[1]")
    @Namespace(reference = "http://tempuri.org/")
    private String password;

    public SehirlerRequest(int ulkeId, String username, String password) {
        this.ulkeId = ulkeId;
        this.username = username;
        this.password = password;
    }
}
