package com.burakkal.imsakiye16.data.remote.model.ilceler;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Namespace;
import org.simpleframework.xml.NamespaceList;
import org.simpleframework.xml.Order;
import org.simpleframework.xml.Path;
import org.simpleframework.xml.Root;

/**
 * Created by Burak on 25.4.2018.
 * burakkal54@gmail.com
 */
@Root(name = "s:Envelope")
@NamespaceList({
        @Namespace(reference = "http://schemas.xmlsoap.org/soap/envelope/", prefix = "s"),
        @Namespace(reference = "http://tempuri.org/", prefix = "t")
})
@Order(elements = {"s:Body/t:Ilceler[1]/EyaletSehirID",
        "s:Body/t:Ilceler[1]/username", "s:Body/t:Ilceler[1]/password"})
public class IlcelerRequest {

    @Element(name = "EyaletSehirID")
    @Path(value = "s:Body/t:Ilceler[1]")
    @Namespace(reference = "http://tempuri.org/")
    private int sehirId;

    @Element
    @Path(value = "s:Body/t:Ilceler[1]")
    @Namespace(reference = "http://tempuri.org/")
    private String username;

    @Element
    @Path(value = "s:Body/t:Ilceler[1]")
    @Namespace(reference = "http://tempuri.org/")
    private String password;

    public IlcelerRequest(int sehirId, String username, String password) {
        this.sehirId = sehirId;
        this.username = username;
        this.password = password;
    }
}
