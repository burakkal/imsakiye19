package com.burakkal.imsakiye16.data.local.entity;

import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.PrimaryKey;

/**
 * Created by Burak on 14.5.2018.
 * burakkal54@gmail.com
 */
@Entity(foreignKeys = @ForeignKey(entity = TblKonum.class,
        parentColumns = "ilceId",
        childColumns = "ilceId",
        onDelete = ForeignKey.CASCADE))
public class TblBayramNamazi {

    @PrimaryKey(autoGenerate = true) private long id;
    private String ramazanHicriTarih;
    private String ramazanTarih;
    private String ramazanSaati;
    private String kurbanHicriTarih;
    private String kurbanTarih;
    private String kurbanSaati;
    private int ilceId;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getRamazanHicriTarih() {
        return ramazanHicriTarih;
    }

    public void setRamazanHicriTarih(String ramazanHicriTarih) {
        this.ramazanHicriTarih = ramazanHicriTarih;
    }

    public String getRamazanTarih() {
        return ramazanTarih;
    }

    public void setRamazanTarih(String ramazanTarih) {
        this.ramazanTarih = ramazanTarih;
    }

    public String getRamazanSaati() {
        return ramazanSaati;
    }

    public void setRamazanSaati(String ramazanSaati) {
        this.ramazanSaati = ramazanSaati;
    }

    public String getKurbanHicriTarih() {
        return kurbanHicriTarih;
    }

    public void setKurbanHicriTarih(String kurbanHicriTarih) {
        this.kurbanHicriTarih = kurbanHicriTarih;
    }

    public String getKurbanTarih() {
        return kurbanTarih;
    }

    public void setKurbanTarih(String kurbanTarih) {
        this.kurbanTarih = kurbanTarih;
    }

    public String getKurbanSaati() {
        return kurbanSaati;
    }

    public void setKurbanSaati(String kurbanSaati) {
        this.kurbanSaati = kurbanSaati;
    }

    public int getIlceId() {
        return ilceId;
    }

    public void setIlceId(int ilceId) {
        this.ilceId = ilceId;
    }
}
