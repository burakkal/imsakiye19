package com.burakkal.imsakiye16.data.remote.model.ilceler;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Path;
import org.simpleframework.xml.Root;

/**
 * Created by Burak on 11.5.2018.
 * burakkal54@gmail.com
 */
@Root(name = "Envelope", strict = false)
public class IlceDetayResponse {

    @Element(name = "CografiKibleAcisi")
    @Path(value = "Body/IlceBilgisiDetayResponse/IlceBilgisiDetayResult")
    private int cografiKibleAcisi;

    @Element(name = "KibleAcisi")
    @Path(value = "Body/IlceBilgisiDetayResponse/IlceBilgisiDetayResult")
    private int kibleAcisi;

    @Element(name = "KabeyeUzaklik")
    @Path(value = "Body/IlceBilgisiDetayResponse/IlceBilgisiDetayResult")
    private int kabeyeUzaklik;

    public int getCografiKibleAcisi() {
        return cografiKibleAcisi;
    }

    public void setCografiKibleAcisi(int cografiKibleAcisi) {
        this.cografiKibleAcisi = cografiKibleAcisi;
    }

    public int getKibleAcisi() {
        return kibleAcisi;
    }

    public void setKibleAcisi(int kibleAcisi) {
        this.kibleAcisi = kibleAcisi;
    }

    public int getKabeyeUzaklik() {
        return kabeyeUzaklik;
    }

    public void setKabeyeUzaklik(int kabeyeUzaklik) {
        this.kabeyeUzaklik = kabeyeUzaklik;
    }
}
