package com.burakkal.imsakiye16.data.local.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.burakkal.imsakiye16.data.local.entity.TblNamazVakti;

import java.util.Date;
import java.util.List;

import io.reactivex.Single;

/**
 * Created by Burak on 14.5.2018.
 * burakkal54@gmail.com
 */
@Dao
public interface NamazVaktiDao {

    @Query("SELECT * FROM tblnamazvakti WHERE ilceId = :ilceId")
    Single<List<TblNamazVakti>> getAylikVakitler(int ilceId);

    @Query("SELECT * FROM tblnamazvakti WHERE ilceId = :ilceId AND miladiTarih >= :from")
    Single<List<TblNamazVakti>> getAylikVakitler(int ilceId, Date from);

    @Query("SELECT * FROM tblnamazvakti WHERE ilceId = :ilceId AND miladiTarih == :date")
    Single<TblNamazVakti> getGunlukVakitlerByIlceId(Date date, int ilceId);

    @Query("SELECT imsak FROM tblnamazvakti WHERE ilceId = :ilceId AND imsak > :date ORDER BY imsak ASC LIMIT 1")
    Single<Date> getSiradakiImsak(int ilceId, Date date);

    @Query("SELECT aksam FROM tblnamazvakti WHERE ilceId = :ilceId AND aksam > :date ORDER BY aksam ASC LIMIT 1")
    Single<Date> getSiradakiAksam(int ilceId, Date date);

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insert(TblNamazVakti... namazVakti);

    @Query("DELETE FROM tblnamazvakti WHERE ilceId = :ilceId")
    void deleteByIlceId(int ilceId);
}
