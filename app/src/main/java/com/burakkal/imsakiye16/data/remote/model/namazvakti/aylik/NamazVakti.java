package com.burakkal.imsakiye16.data.remote.model.namazvakti.aylik;

import com.burakkal.imsakiye16.data.local.entity.TblNamazVakti;
import com.burakkal.imsakiye16.util.DateUtils;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

import java.text.ParseException;

/**
 * Created by Burak on 13.5.2018.
 * burakkal54@gmail.com
 */
@Root(name = "NamazVakti", strict = false)
public class NamazVakti {

    @Element(name = "Imsak") private String imsak;
    @Element(name = "Gunes") private String gunes;
    @Element(name = "Ogle") private String ogle;
    @Element(name = "Ikindi") private String ikindi;
    @Element(name = "Aksam") private String aksam;
    @Element(name = "Yatsi") private String yatsi;
    @Element(name = "GunesBatis") private String gunesBatis;
    @Element(name = "GunesDogus") private String gunesDogus;
    @Element(name = "KibleSaati") private String kibleSaati;
    @Element(name = "HicriTarihUzun") private String hicriTarih;
    @Element(name = "MiladiTarihKisa") private String miladiTarih;
    @Element(name = "AyinSekliURL") private String ayinSekliUrl;

    public String getImsak() {
        return imsak;
    }

    public void setImsak(String imsak) {
        this.imsak = imsak;
    }

    public String getGunes() {
        return gunes;
    }

    public void setGunes(String gunes) {
        this.gunes = gunes;
    }

    public String getOgle() {
        return ogle;
    }

    public void setOgle(String ogle) {
        this.ogle = ogle;
    }

    public String getIkindi() {
        return ikindi;
    }

    public void setIkindi(String ikindi) {
        this.ikindi = ikindi;
    }

    public String getAksam() {
        return aksam;
    }

    public void setAksam(String aksam) {
        this.aksam = aksam;
    }

    public String getYatsi() {
        return yatsi;
    }

    public void setYatsi(String yatsi) {
        this.yatsi = yatsi;
    }

    public String getGunesBatis() {
        return gunesBatis;
    }

    public void setGunesBatis(String gunesBatis) {
        this.gunesBatis = gunesBatis;
    }

    public String getGunesDogus() {
        return gunesDogus;
    }

    public void setGunesDogus(String gunesDogus) {
        this.gunesDogus = gunesDogus;
    }

    public String getKibleSaati() {
        return kibleSaati;
    }

    public void setKibleSaati(String kibleSaati) {
        this.kibleSaati = kibleSaati;
    }

    public String getHicriTarih() {
        return hicriTarih;
    }

    public void setHicriTarih(String hicriTarih) {
        this.hicriTarih = hicriTarih;
    }

    public String getMiladiTarih() {
        return miladiTarih;
    }

    public void setMiladiTarih(String miladiTarih) {
        this.miladiTarih = miladiTarih;
    }

    public String getAyinSekliUrl() {
        return ayinSekliUrl;
    }

    public void setAyinSekliUrl(String ayinSekliUrl) {
        this.ayinSekliUrl = ayinSekliUrl;
    }

    public TblNamazVakti toTblNamazVakti(int ilceId) throws ParseException {
        TblNamazVakti tblNamazVakti = new TblNamazVakti();
        tblNamazVakti.setMiladiTarih(DateUtils.toDate(miladiTarih));
        tblNamazVakti.setImsak(DateUtils.toDate(miladiTarih, imsak));
        tblNamazVakti.setGunes(DateUtils.toDate(miladiTarih, gunes));
        tblNamazVakti.setOgle(DateUtils.toDate(miladiTarih, ogle));
        tblNamazVakti.setIkindi(DateUtils.toDate(miladiTarih, ikindi));
        tblNamazVakti.setAksam(DateUtils.toDate(miladiTarih, aksam));
        tblNamazVakti.setYatsi(DateUtils.toDate(miladiTarih, yatsi));
        tblNamazVakti.setGunesBatis(DateUtils.toDate(miladiTarih, gunesBatis));
        tblNamazVakti.setGunesDogus(DateUtils.toDate(miladiTarih, gunesDogus));
        tblNamazVakti.setKibleSaati(DateUtils.toDate(miladiTarih, kibleSaati));
        tblNamazVakti.setHicriTarih(hicriTarih);
        tblNamazVakti.setAyinSekliUrl(ayinSekliUrl);
        tblNamazVakti.setIlceId(ilceId);
        return tblNamazVakti;
    }
}
