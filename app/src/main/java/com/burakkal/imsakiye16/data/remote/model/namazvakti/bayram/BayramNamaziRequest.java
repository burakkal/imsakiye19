package com.burakkal.imsakiye16.data.remote.model.namazvakti.bayram;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Namespace;
import org.simpleframework.xml.NamespaceList;
import org.simpleframework.xml.Order;
import org.simpleframework.xml.Path;
import org.simpleframework.xml.Root;

/**
 * Created by Burak on 11.5.2018.
 * burakkal54@gmail.com
 */
@Root(name = "s:Envelope")
@NamespaceList({
        @Namespace(reference = "http://schemas.xmlsoap.org/soap/envelope/", prefix = "s"),
        @Namespace(reference = "http://tempuri.org/", prefix = "t")
})
@Order(elements = {"s:Body/t:BayramNamaziVakti[1]/IlceID",
        "s:Body/t:BayramNamaziVakti[1]/username", "s:Body/t:BayramNamaziVakti[1]/password"})
public class BayramNamaziRequest {

    @Element(name = "IlceID")
    @Path(value = "s:Body/t:BayramNamaziVakti[1]")
    @Namespace(reference = "http://tempuri.org/")
    private int ilceId;

    @Element
    @Path(value = "s:Body/t:BayramNamaziVakti[1]")
    @Namespace(reference = "http://tempuri.org/")
    private String username;

    @Element
    @Path(value = "s:Body/t:BayramNamaziVakti[1]")
    @Namespace(reference = "http://tempuri.org/")
    private String password;

    public BayramNamaziRequest(int ilceId, String username, String password) {
        this.ilceId = ilceId;
        this.username = username;
        this.password = password;
    }
}
