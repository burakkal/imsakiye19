package com.burakkal.imsakiye16.data.remote.model.sehirler;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

import java.io.Serializable;

/**
 * Created by Burak on 25.4.2018.
 * burakkal54@gmail.com
 */
@Root(name = "Sehir")
public class Sehir implements Serializable {

    @Element(name = "SehirAdi")
    private String sehirAdi;

    @Element(name = "SehirAdiEn")
    private String sehirAdiEn;

    @Element(name = "SehirID")
    private int sehirId;

    public String getSehirAdi() {
        return sehirAdi;
    }

    public void setSehirAdi(String sehirAdi) {
        this.sehirAdi = sehirAdi;
    }

    public String getSehirAdiEn() {
        return sehirAdiEn;
    }

    public void setSehirAdiEn(String sehirAdiEn) {
        this.sehirAdiEn = sehirAdiEn;
    }

    public int getSehirId() {
        return sehirId;
    }

    public void setSehirId(int sehirId) {
        this.sehirId = sehirId;
    }
}
