package com.burakkal.imsakiye16.data.pref.base;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import androidx.annotation.StringRes;

/**
 * Created by Burak on 27.4.2018.
 * burakkal54@gmail.com
 */
public class BasePreferences {

    protected Context context;
    private SharedPreferences sharedPreferences;

    public BasePreferences(Context context) {
        this.context = context;
        this.sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
    }

    protected String getString(@StringRes int key, @StringRes int defValue) {
        return getString(context.getString(key), context.getString(defValue));
    }

    protected String getString(@StringRes int key, String defValue) {
        return getString(context.getString(key), defValue);
    }

    protected String getString(String key, String defValue) {
        return sharedPreferences.getString(key, defValue);
    }

    protected void setString(@StringRes int key, String value) {
        setString(context.getString(key), value);
    }

    protected void setString(String key, String value) {
        sharedPreferences.edit()
                .putString(key, value)
                .apply();
    }

    protected boolean getBoolean(@StringRes int key, boolean defValue) {
        return getBoolean(context.getString(key), defValue);
    }

    protected boolean getBoolean(String key, boolean defValue) {
        return sharedPreferences.getBoolean(key, defValue);
    }

    protected void setBoolean(@StringRes int key, boolean value) {
        setBoolean(context.getString(key), value);
    }

    protected void setBoolean(String key, boolean value) {
        sharedPreferences.edit()
                .putBoolean(key, value)
                .apply();
    }
}
