package com.burakkal.imsakiye16.data.remote;

import com.burakkal.imsakiye16.data.remote.model.ilceler.IlceDetayRequest;
import com.burakkal.imsakiye16.data.remote.model.ilceler.IlceDetayResponse;
import com.burakkal.imsakiye16.data.remote.model.ilceler.IlcelerRequest;
import com.burakkal.imsakiye16.data.remote.model.ilceler.IlcelerResponse;
import com.burakkal.imsakiye16.data.remote.model.namazvakti.aylik.AylikNamazVaktiRequest;
import com.burakkal.imsakiye16.data.remote.model.namazvakti.aylik.AylikNamazVaktiResponse;
import com.burakkal.imsakiye16.data.remote.model.namazvakti.bayram.BayramNamaziRequest;
import com.burakkal.imsakiye16.data.remote.model.namazvakti.bayram.BayramNamaziResponse;
import com.burakkal.imsakiye16.data.remote.model.namazvakti.imsakiye.ImsakiyeRequest;
import com.burakkal.imsakiye16.data.remote.model.namazvakti.imsakiye.ImsakiyeResponse;
import com.burakkal.imsakiye16.data.remote.model.sehirler.SehirlerRequest;
import com.burakkal.imsakiye16.data.remote.model.sehirler.SehirlerResponse;
import com.burakkal.imsakiye16.data.remote.model.ulkeler.UlkelerRequest;
import com.burakkal.imsakiye16.data.remote.model.ulkeler.UlkelerResponse;

import io.reactivex.Single;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;

/**
 * Created by Burak on 23.4.2018.
 * burakkal54@gmail.com
 */
public interface DiyanetApi {

    @POST("wsNamazVakti.svc/")
    @Headers({
            "Content-Type: text/xml",
            "SOAPAction: http://tempuri.org/IwsNamazVakti/Ulkeler"
    })
    Single<UlkelerResponse> getUlkeler(@Body UlkelerRequest ulkelerRequest);

    @POST("wsNamazVakti.svc/")
    @Headers({
            "Content-Type: text/xml",
            "SOAPAction: http://tempuri.org/IwsNamazVakti/Sehirler"
    })
    Single<SehirlerResponse> getSehirler(@Body SehirlerRequest sehirlerRequest);

    @POST("wsNamazVakti.svc/")
    @Headers({
            "Content-Type: text/xml",
            "SOAPAction: http://tempuri.org/IwsNamazVakti/Ilceler"
    })
    Single<IlcelerResponse> getIlceler(@Body IlcelerRequest ilcelerRequest);

    @POST("wsNamazVakti.svc/")
    @Headers({
            "Content-Type: text/xml",
            "SOAPAction: http://tempuri.org/IwsNamazVakti/IlceBilgisiDetay"
    })
    Single<IlceDetayResponse> getIlceDetay(@Body IlceDetayRequest ilceDetayRequest);

    @POST("wsNamazVakti.svc/")
    @Headers({
            "Content-Type: text/xml",
            "SOAPAction: http://tempuri.org/IwsNamazVakti/AylikNamazVakti"
    })
    Single<AylikNamazVaktiResponse> getAylikNamazVakitleri(
            @Body AylikNamazVaktiRequest aylikNamazVaktiRequest);

    @POST("wsNamazVakti.svc/")
    @Headers({
            "Content-Type: text/xml",
            "SOAPAction: http://tempuri.org/IwsNamazVakti/Imsakiye"
    })
    Single<ImsakiyeResponse> getImsakiye(@Body ImsakiyeRequest imsakiyeRequest);

    @POST("wsNamazVakti.svc/")
    @Headers({
            "Content-Type: text/xml",
            "SOAPAction: http://tempuri.org/IwsNamazVakti/BayramNamaziVakti"
    })
    Single<BayramNamaziResponse> getBayramNamazi(@Body BayramNamaziRequest bayramNamaziRequest);
}
