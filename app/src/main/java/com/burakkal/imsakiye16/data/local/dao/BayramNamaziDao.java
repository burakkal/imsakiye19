package com.burakkal.imsakiye16.data.local.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import com.burakkal.imsakiye16.data.local.entity.TblBayramNamazi;

import io.reactivex.Single;

/**
 * Created by Burak on 14.5.2018.
 * burakkal54@gmail.com
 */
@Dao
public interface BayramNamaziDao {

    @Query("SELECT * FROM tblbayramnamazi WHERE ilceId = :ilceId")
    Single<TblBayramNamazi> getBayramNamaziByIlceId(int ilceId);

    @Insert
    void insert(TblBayramNamazi... bayramNamazi);
}
