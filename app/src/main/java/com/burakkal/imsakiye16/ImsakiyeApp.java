package com.burakkal.imsakiye16;

import android.app.Activity;
import android.app.Application;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.util.Log;

import com.burakkal.imsakiye16.di.components.AppComponent;
import com.burakkal.imsakiye16.di.components.DaggerAppComponent;
import com.burakkal.imsakiye16.di.modules.AppContextModule;
import com.burakkal.imsakiye16.di.modules.DatabaseModule;
import com.burakkal.imsakiye16.di.modules.NetworkModule;
import com.burakkal.imsakiye16.di.modules.PreferencesModule;
import com.google.android.gms.ads.MobileAds;

import androidx.appcompat.app.AppCompatDelegate;
import androidx.multidex.MultiDex;
import androidx.multidex.MultiDexApplication;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import timber.log.Timber;

/**
 * Created by Burak on 23.4.2018.
 * burakkal54@gmail.com
 */
public class ImsakiyeApp extends MultiDexApplication {

    private AppComponent component;

    public static ImsakiyeApp get(Activity activity) {
        return (ImsakiyeApp) activity.getApplication();
    }

    public static ImsakiyeApp get(Service service) {
        return (ImsakiyeApp) service.getApplication();
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        if (BuildConfig.DEBUG) Timber.plant(new Timber.DebugTree());

        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
        MobileAds.initialize(this, "ca-app-pub-5434825563465092~4317629563");

        component = DaggerAppComponent.builder()
                .appContextModule(new AppContextModule(this))
                .networkModule(new NetworkModule())
                .preferencesModule(new PreferencesModule())
                .databaseModule(new DatabaseModule())
                .build();
    }

    public AppComponent getComponent() {
        return component;
    }
}
