package com.burakkal.imsakiye16.receiver

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.util.Log
import com.burakkal.imsakiye16.ImsakiyeApp
import com.burakkal.imsakiye16.data.pref.hatirlatici.HatirlaticiPreferences
import com.burakkal.imsakiye16.ui.alarm.AlarmHelper
import javax.inject.Inject

class BootReceiver: BroadcastReceiver(), AlarmHelper.CompletionListener {

    @Inject lateinit var hatirlaticiPref: HatirlaticiPreferences
    @Inject lateinit var alarmHelper: AlarmHelper

    override fun onReceive(context: Context?, intent: Intent?) {
        val imsakiyeApp = context?.applicationContext as ImsakiyeApp
        imsakiyeApp.component.inject(this)
        when(intent?.action) {
            Intent.ACTION_BOOT_COMPLETED -> setAlarms()
        }
    }

    private fun setAlarms() {
        Log.d("BootReceiver", "setAlarms")
        // TODO: BUG: sahur uyandırma bazen kurulmuyor
        alarmHelper.completionListener = this
        if (hatirlaticiPref.isSahurUyandirmaChecked) alarmHelper.setAlarmSahurUyandirma()
        if (hatirlaticiPref.isImsakUyariChecked) alarmHelper.setAlarmImsakUyari()
        if (hatirlaticiPref.isIftarUyariChecked) alarmHelper.setAlarmIftarUyari()
    }

    override fun onComplete() {
        Log.d("BootReceiver", "onComplete")
        alarmHelper.unsubscribe()
    }

    override fun onError(error: Throwable) {
        Log.e("BootReceiver", "onError:" + error.stackTrace.toString())
        alarmHelper.unsubscribe()
    }

}