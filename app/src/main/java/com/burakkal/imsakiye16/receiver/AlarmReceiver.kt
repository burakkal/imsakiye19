package com.burakkal.imsakiye16.receiver

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import com.burakkal.imsakiye16.data.ImsakiyeRepository
import com.burakkal.imsakiye16.data.local.entity.TblNamazVakti
import com.burakkal.imsakiye16.ui.alarm.AlarmActivity
import com.burakkal.imsakiye16.util.DateUtils
import io.reactivex.schedulers.Schedulers
import timber.log.Timber
import javax.inject.Inject

class AlarmReceiver: BroadcastReceiver() {

    override fun onReceive(context: Context?, intent: Intent?) {
        val vakitRequestCode = intent?.getIntExtra("VakitHatirlaticiRequestCode", -1)

        // TODO: convert to wakefulbroadcastreceiver
        // https://stackoverflow.com/questions/51532059/android-gcm-wakefulbroadcastreceiver-startwakefulservice-crash-due-to-illegals

        val i = Intent(context, AlarmActivity::class.java).apply {
            putExtra("VakitHatirlaticiRequestCode", vakitRequestCode)
            flags = Intent.FLAG_ACTIVITY_NEW_TASK
        }
        context?.startActivity(i)
    }
}