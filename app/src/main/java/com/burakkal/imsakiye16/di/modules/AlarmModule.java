package com.burakkal.imsakiye16.di.modules;

import android.content.Context;

import com.burakkal.imsakiye16.data.local.ImsakiyeDatabase;
import com.burakkal.imsakiye16.data.pref.hatirlatici.HatirlaticiPreferences;
import com.burakkal.imsakiye16.ui.alarm.AlarmHelper;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module(includes = {AppContextModule.class, DatabaseModule.class, PreferencesModule.class})
public class AlarmModule {

    @Singleton
    @Provides
    AlarmHelper provideAlarmHelper(Context context, ImsakiyeDatabase db, HatirlaticiPreferences pref) {
        return new AlarmHelper(context, db, pref);
    }
}
