package com.burakkal.imsakiye16.di.modules;

import android.content.Context;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Burak on 27.4.2018.
 * burakkal54@gmail.com
 */
@Module
public class AppContextModule {

    private Context context;

    public AppContextModule(Context context) {
        this.context = context.getApplicationContext();
    }

    @Singleton
    @Provides
    Context provideAppContext() {
        return context;
    }
}
