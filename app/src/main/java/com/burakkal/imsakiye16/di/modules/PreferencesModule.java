package com.burakkal.imsakiye16.di.modules;

import android.content.Context;

import com.burakkal.imsakiye16.data.pref.app.AppPreferences;
import com.burakkal.imsakiye16.data.pref.hatirlatici.HatirlaticiPreferences;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Burak on 27.4.2018.
 * burakkal54@gmail.com
 */
@Module(includes = AppContextModule.class)
public class PreferencesModule {

    @Provides
    AppPreferences provideAppPreferences(Context context) {
        return new AppPreferences(context);
    }

    @Provides
    HatirlaticiPreferences provideHatirlaticiPreferences(Context context) {
        return new HatirlaticiPreferences(context);
    }
}
