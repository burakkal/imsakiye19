package com.burakkal.imsakiye16.di.modules;

import com.burakkal.imsakiye16.data.ImsakiyeRepository;
import com.burakkal.imsakiye16.data.local.ImsakiyeDatabase;
import com.burakkal.imsakiye16.data.remote.DiyanetApi;

import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.convert.AnnotationStrategy;
import org.simpleframework.xml.core.Persister;
import org.simpleframework.xml.strategy.Strategy;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.simplexml.SimpleXmlConverterFactory;

/**
 * Created by Burak on 23.4.2018.
 * burakkal54@gmail.com
 */
@Module(includes = DatabaseModule.class)
public class NetworkModule {

    @Singleton
    @Provides
    ImsakiyeRepository provideImsakiyeRepository(DiyanetApi diyanetApi, ImsakiyeDatabase imsakiyeDb) {
        return new ImsakiyeRepository(diyanetApi, imsakiyeDb);
    }

    @Singleton
    @Provides
    DiyanetApi provideDiyanetApi(Retrofit retrofit) {
        return retrofit.create(DiyanetApi.class);
    }

    @Singleton
    @Provides
    Retrofit provideRetrofit(Serializer serializer) {
        return new Retrofit.Builder()
                .baseUrl("http://namazvakti.diyanet.gov.tr/")
                .addConverterFactory(SimpleXmlConverterFactory.create(serializer))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();
    }

    @Provides
    Serializer provideSerializer(Strategy strategy) {
        return new Persister(strategy);
    }

    @Provides
    Strategy provideStrategy() {
        return new AnnotationStrategy();
    }
}
