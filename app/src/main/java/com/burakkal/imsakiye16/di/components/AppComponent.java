package com.burakkal.imsakiye16.di.components;

import com.burakkal.imsakiye16.di.modules.AlarmModule;
import com.burakkal.imsakiye16.di.modules.AppContextModule;
import com.burakkal.imsakiye16.di.modules.DatabaseModule;
import com.burakkal.imsakiye16.di.modules.PreferencesModule;
import com.burakkal.imsakiye16.receiver.AlarmReceiver;
import com.burakkal.imsakiye16.receiver.BootReceiver;
import com.burakkal.imsakiye16.ui.alarm.AlarmActivity;
import com.burakkal.imsakiye16.ui.ayarlar.AyarlarFragment;
import com.burakkal.imsakiye16.ui.ayarlar.BaslangicAyarlarActivity;
import com.burakkal.imsakiye16.ui.aylikgorunum.AylikGorunumActivity;
import com.burakkal.imsakiye16.ui.main.MainActivity;
import com.burakkal.imsakiye16.di.modules.NetworkModule;
import com.burakkal.imsakiye16.ui.sehirsec.ilce.IlceSecFragment;
import com.burakkal.imsakiye16.ui.sehirsec.sehir.SehirSecFragment;
import com.burakkal.imsakiye16.ui.sehirsec.ulke.UlkeSecFragment;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by Burak on 23.4.2018.
 * burakkal54@gmail.com
 */
@Singleton
@Component(modules = {AppContextModule.class, NetworkModule.class, PreferencesModule.class,
        DatabaseModule.class, AlarmModule.class})
public interface AppComponent {

    void inject(MainActivity mainActivity);
    void inject(UlkeSecFragment ulkeSecFragment);
    void inject(SehirSecFragment sehirSecFragment);
    void inject(IlceSecFragment ilceSecFragment);
    void inject(AylikGorunumActivity aylikGorunumActivity);
    void inject(AyarlarFragment ayarlarFragment);
    void inject(BaslangicAyarlarActivity baslangicAyarlarActivity);
    void inject(AlarmReceiver alarmReceiver);
    void inject(AlarmActivity alarmActivity);
    void inject(BootReceiver bootReceiver);
}
