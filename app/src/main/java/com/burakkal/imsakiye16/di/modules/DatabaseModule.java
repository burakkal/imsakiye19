package com.burakkal.imsakiye16.di.modules;

import androidx.room.Room;
import android.content.Context;

import com.burakkal.imsakiye16.data.local.ImsakiyeDatabase;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Burak on 9.5.2018.
 * burakkal54@gmail.com
 */
@Module(includes = AppContextModule.class)
public class DatabaseModule {

    @Singleton
    @Provides
    ImsakiyeDatabase provideImsakiyeDb(Context context) {
        return Room.databaseBuilder(context, ImsakiyeDatabase.class, "Imsakiye.db")
                .fallbackToDestructiveMigration()
                .build();
    }
}
