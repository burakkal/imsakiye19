package com.burakkal.imsakiye16.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import timber.log.Timber;

/**
 * Created by Burak on 15.5.2018.
 * burakkal54@gmail.com
 */
public class DateUtils {

    public static String FORMAT_YEAR = "dd.MM.yyyy";
    public static String FORMAT_YEAR_AND_HOUR = "dd.MM.yyyy HH:mm";
    public static String FORMAT_HOUR = "HH:mm";

    public static Date toDate(String ddMMyyyy) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
        return sdf.parse(ddMMyyyy);
    }

    public static Date toDate(String ddMMyyyy, String HHmm) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy HH:mm");
        String dH = ddMMyyyy + " " + HHmm;
        return sdf.parse(dH);
    }

    public static String fromDate(Date date, String format) {
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        return sdf.format(date);
    }

    public static Date getToday() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        Timber.i(calendar.getTimeInMillis() + " : " + fromDate(calendar.getTime(), FORMAT_YEAR_AND_HOUR));
        return calendar.getTime();
    }

    public static Date getTomorrow() {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(getToday());
        calendar.add(Calendar.DAY_OF_YEAR, 1);
        return calendar.getTime();
    }

    public static String fromElapsedTime(long ms) {
        long days = TimeUnit.MILLISECONDS.toDays(ms);
        ms -= TimeUnit.DAYS.toMillis(days);
        long hours = TimeUnit.MILLISECONDS.toHours(ms);
        ms -= TimeUnit.HOURS.toMillis(hours);
        long minutes = TimeUnit.MILLISECONDS.toMinutes(ms);
        ms -= TimeUnit.MINUTES.toMillis(minutes);
        long seconds = TimeUnit.MILLISECONDS.toSeconds(ms);
        if (days > 0) return String.format("%02d:%02d:%02d:%02d",days,hours,minutes,seconds);
        else return String.format("%02d:%02d:%02d",hours,minutes,seconds);
    }

    public static boolean hasDatePassed(Date date) {
        return date.before(getToday());
    }
}
