package com.burakkal.imsakiye16.util

import android.app.AlarmManager
import android.app.PendingIntent
import android.content.Context
import android.content.Context.ALARM_SERVICE
import android.content.Intent
import android.os.Build
import com.burakkal.imsakiye16.receiver.AlarmReceiver
import com.burakkal.imsakiye16.ui.ayarlar.VakitHatirlatici
import timber.log.Timber
import java.util.*

fun alarmAyarla(context: Context, vakitHatirlatici: VakitHatirlatici,
                tarih: Date?, sonrakiTarih: Date?) {
    if (tarih == null) return
    val alarmMgr = context.getSystemService(ALARM_SERVICE) as AlarmManager
    val alarmIntent = Intent(context, AlarmReceiver::class.java).let { intent ->
        intent.putExtra("VakitHatirlaticiRequestCode", vakitHatirlatici.code)
        PendingIntent.getBroadcast(context, vakitHatirlatici.code, intent, 0)
    }
    val calendar = Calendar.getInstance().apply { time = tarih }
    if (calendar.timeInMillis < System.currentTimeMillis()) {
        if (sonrakiTarih == null) return
        else calendar.time = sonrakiTarih
    }
    // COMPLETED: setExactAndAllowWhileIdle() bak, https://developer.android.com/training/scheduling/alarms#type
    // https://developer.android.com/reference/android/app/AlarmManager.html#setExactAndAllowWhileIdle(int,%20long,%20android.app.PendingIntent)
    // https://developer.android.com/training/monitoring-device-state/doze-standby
    when {
        Build.VERSION.SDK_INT >= Build.VERSION_CODES.M ->
            alarmMgr.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, calendar.timeInMillis, alarmIntent)
        Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT ->
            alarmMgr.setExact(AlarmManager.RTC_WAKEUP, calendar.timeInMillis, alarmIntent)
        else ->
            alarmMgr.set(AlarmManager.RTC_WAKEUP, calendar.timeInMillis, alarmIntent)
    }
}

fun alarmAyarla(context: Context, vakitHatirlatici: VakitHatirlatici, tarih: Date?) {
    if (tarih == null) return
    val alarmMgr = context.getSystemService(ALARM_SERVICE) as AlarmManager
    val alarmIntent = Intent(context, AlarmReceiver::class.java).let { intent ->
        intent.putExtra("VakitHatirlaticiRequestCode", vakitHatirlatici.code)
        PendingIntent.getBroadcast(context, vakitHatirlatici.code, intent, 0)
    }
    val calendar = Calendar.getInstance().apply { time = tarih }
    when {
        Build.VERSION.SDK_INT >= Build.VERSION_CODES.M ->
            alarmMgr.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, calendar.timeInMillis, alarmIntent)
        Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT ->
            alarmMgr.setExact(AlarmManager.RTC_WAKEUP, calendar.timeInMillis, alarmIntent)
        else ->
            alarmMgr.set(AlarmManager.RTC_WAKEUP, calendar.timeInMillis, alarmIntent)
    }
    Timber.i("%s için %s tarihine alarm kuruldu",
            vakitHatirlatici.toString(), DateUtils.fromDate(tarih, DateUtils.FORMAT_YEAR_AND_HOUR))
}

fun alarmIptal(context: Context, vakitHatirlatici: VakitHatirlatici) {
    val alarmMgr = context.getSystemService(ALARM_SERVICE) as AlarmManager
    val alarmIntent = Intent(context, AlarmReceiver::class.java).let { intent ->
        PendingIntent.getBroadcast(context, vakitHatirlatici.code, intent, 0)
    }
    alarmMgr.cancel(alarmIntent)
    alarmIntent.cancel()
    Timber.i("%s için alarm iptal edildi", vakitHatirlatici.toString())
}