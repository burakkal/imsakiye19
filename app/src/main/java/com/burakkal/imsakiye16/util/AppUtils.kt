package com.burakkal.imsakiye16.util

import android.content.ContentResolver
import android.content.Context
import android.net.Uri
import android.widget.Toast
import androidx.annotation.RawRes

fun getRawUri(@RawRes rawRes: Int, context: Context): Uri =
        Uri.parse("${ContentResolver.SCHEME_ANDROID_RESOURCE}://${context.packageName}/$rawRes")

fun showToast(context: Context?, message: String) {
    if (context == null) return
    Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
}